#pragma once
#include <list>

namespace ui {
	enum class eventtype	{ motion, key, text, click, scroll };
	enum btnstate			{ pressed, released };
	struct event_motion {
		unsigned int x, y;
		signed int dx, dy;
	};
	struct event_key {
		int				key;
		enum btnstate	state;
	};
	struct event_click {
		enum { left, middle, right }	btn;
		enum btnstate					state;
	};
	struct event_scroll {
		int delta;
	};
	struct event {		// this is when I finally figured out the right way to do tagged enums.
		eventtype type;
		union {
			struct event_motion motion;
			struct event_key key;
			struct event_click click;
			struct event_scroll scroll;
		};
	};
	class Group;
	class Widget { public:
		enum State {off, hover, active, focus} state;

		float x, y;
		bool visible;
		Group* parent;
		float abx() const, aby() const;
		Widget();
		virtual bool inhitbox(float x, float y);
		virtual void	render()				= 0, // look, ma, no implementation!
						loop()					= 0,
						handle(const event&)	= 0;
		virtual ~Widget();
		virtual void onBlur();
	};
	class Group : public Widget { public:
		float w, h;
		std::list<Widget*> widgets;
		Widget* hoverwidget,
		      * activewidget,
			  * focuswidget;
		Group();
		virtual ~Group();
		// C++ apparently needs virtual destructors for anything whose pointer type might
		// be used to deallocate a child class, because C++, despite having a
		// Turing-complete metaprogramming system, cannot be arsed to implement
		// destructors sanely.
		// I AM DISAPPOINT.
		virtual void render();
		virtual void loop();
		virtual void handle(const event& e);
		virtual void callback(void*);
		virtual void auxcallback(void*); // for right-clicking
		virtual void add(Widget* w);
		void defocus();
		void focusOn(Widget*);
		virtual bool inhitbox(float x, float y);
	};
	class ReverseGroup : public Group { //handle inverted hitboxes
		bool inhitbox(float x, float y) override;
	};

	// controllers are identical to groups but automatically mark
	// themselves as the root node in the UI tree.
	class Controller : public Group { public:
		Controller();
		virtual ~Controller();
		virtual void reactivate(void* v);
		bool inhitbox(float x, float y);
	};
}
