#pragma once
#include <string>
#include <list>
#include "types.h"
#include "location.h"
namespace data {
	class ship : public location { public:
		struct Class {
			const char* name;
			health_t hull, energy;
			weight_t maxweight;
			float size;
			quantity_t hardpoints_weapon, hardpoints_util, crew;
			struct {
				const char* file;
				unsigned int w, h;
			} image;
			tech_t techlevel;
			const char* desc;
			item* hyperdrive;
		};
		Class* shipclass;
		std::string name;
		health_t hull, energy;
		ship(Class*, std::string name="");
		dist_t range() const;

		item* hyperdrive;
		weight_t weight() const;	// weigh all items
		health_t jumpEnergyCost(dist_t) const;	// I can truthfully say I have invented hyperjump mathematics. For certain values of truthfulness.
		health_t jumpEnergyCost(const system*) const;
		utime_t jumpTimeTo(dist_t) const;
		utime_t jumpTimeTo(const system*) const;
		dist_t distanceTo(const system*) const;
		bool canJumpTo(const system*) const;
		void jumpTo(const system*);
		
	};
}