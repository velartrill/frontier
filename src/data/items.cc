#include "item.h"
namespace data {
	item aegis_items[] = {
		/* Type						price	techlevel	effect	speed	rate	range	recharge */
		{(char*)"Shibiraya",
			(char*)"The word \"Shibiraya\" roughly translates from the Common Standard as \"one who sneaks into parties uninvited.\""
			"The most common civilian hyperdrive in Aegis space, the Shibiraya is renowned for its speed, low power consumption, "
			"and extremely reliability, despite being one of the Aegis' lower-end hyperdrives. Like most Aegis hyperdrives, it is "
			"relatively short-range, designed to maximize traveller comfort rather than jump efficiency; however, the large number "
			"of jumps required to traverse longer differences may put a ship at significantly greater risk of encountering pirates.",
			item::Type::hyperdrive,	7000,	12,			0,		2,		1,		.8,		0},
		{(char*)"Laqrumaya",
			(char*)"From the Common Standard \"one who is needlessly boastful\", the Laqrumaya is a mid-range drive designed with "
			"serial tourists in mind. Significantly faster and longer-range than the Shibiraya, but less energy-efficient.",
			item::Type::hyperdrive,	12500,	12,			0,		3,		1.2,	1.3,	0},
		{(char*)"Setashaya",
			(char*)"Its name meaning \"one who watches smugly from atop a large rock\", the Setashaya is designed for Aegis travellers "
			"who have need to travel outside the Aegis, and as such competes with baseline Imperial and Society drives in utilitarian "
			"terms without giving up civilian comforts.",
			item::Type::hyperdrive,	17000,	14,			0,		5,		.9,		1.8,	0},
		{(char*)"Therrekaya I",
			(char*)"The first of the Aegis non-civilian drive series, the Therrekaya boasts speed and efficiency far superior to "
			"Imperial and most Society hyperdrives. Designed primarily for emergency transport, couriers, and low-end military "
			"operations, but available to civilians, as its name, \"one who subverts feelings of inadequacy\" would seem to indicate.",
			item::Type::hyperdrive,	35300,	16,			0,		15,		.7,		3,		0},
		{(char*)"Therrekaya II",
			(char*)"With massive range and efficiency improvements over its cheaper cousin, the Therrekaya II is a brute-force "
			"upgrade, using the same basic chasis design but with more expensive components.",
			item::Type::hyperdrive,	47500,	16,			0,		18,		.5,		5,		0},
		{(char*)"Roqrataya",
			(char*)"Named \"one who delivers swift vengenace\", the Roqrataya is a military drive used on most Aegis warships. It "
			"provides nearly unparalleled range and speed, though at a cost of efficiency.",
			item::Type::hyperdrive,	64000,	17,			0,		24,		.8,		7,		0},
		{(char*)"Thetasya Rotuenye",
			(char*)"The latest in Aegis hyperdrive technology, the \"swiftly twisting flame\" fully leverages Aegis mastery of "
			"hyperspatial science to deliver extreme speed and range, rivaled only by experimental Society hyperdrives.",
			item::Type::hyperdrive,	250000,	19,			0,		30,		.5,		12,		0},
	
	};

	item empire_items[] = {
		/* Type						price	techlevel	effect	speed	rate	range	recharge */
		{(char*)"Lidraxe RS99",
			(char*)"The butt of ten thousand mean-spirited jokes from engineers the Empire over, the RS99 Hyperdrive, manufactured by the Lidraxe Corporation, is the Empire's most hated and yet most prolific hyperdrive, much for the same reasons that the impossibly dangerous and inaccurate Third-Empire era SM-7 Suppressor Rifle is still the favored firearm of communist rebels and tinpot regimes across the galaxy: it's cheap to manufacture, and has relatively low power requirements. It more than makes up for these advantages by being extremely time-consuming to maintain, requiring on average two hundred recalibrations a year, and parts frequently become dislodged after a successful jump. So-called \"consumer watchdog groups\" have alleged that this drive is prone to causing spontaneous massive existence failures; there is absolutely no evidence of that.",
			item::Type::hyperdrive,	5500,	5,			0,		4,		1.2,		1,	0}
	};

	item society_items[] = {
		/* Type						price	techlevel	effect	speed	rate	range	recharge */
		{(char*)"IHD-105 Falcon",
			(char*)"Perhaps nowhere in the galaxy is there a more neurotic institution than the Society Directorate of Interstellar Engineering. The introduction of the IHD-105 \"Falcon\" required more than a few red-faced scientists to hurriedly issue corrections to their hyperspatial engineering texts, as it managed to achieve a hitherto unheard-of cost-to-efficiency ratio, squeezing every last drop of pinch potential out of its hyperfield motivators.",
			item::Type::hyperdrive,	11000,	7,			0,		6,		.8,		1.6,	0}
	};
}