#include "field.h"
#include "../color.h"
#include <glfw.h>
#include "../util.h"
#include "../window.h"
#include "../tostring.h"
namespace ui {
	Field::Field(float _w, float _size, std::string _text, bool _numeric):
		_text_obj(_text,Font::face::zekton,_size)
	{
		text	=	_text;
		w		=	_w;
		size	=	_size;
		numeric	=	_numeric;
		cursor	=	0;
	}
	Field::Field(float _x, float _y, float _w, float _size, std::string _text, bool _numeric):
		_text_obj(_text,Font::face::zekton,_size)
	{
		x		=	_x;
		y		=	_y;
		text	=	_text;
		w		=	_w;
		size	=	_size;
		numeric	=	_numeric;
		cursor	=	0;
	}
	void Field::loop() {
		cursor += window::ticks();
		if (state==focus && cursor>=.8) cursor=0;
		//$("Cursor: %", cursor);
	}
	void Field::render() {
		float x = abx(), y=aby();
		color::apply(color::blue, state, color::colortype::border);
		glBegin(GL_QUADS);
			glVertex2f(x-1,		y-1);
			glVertex2f(x+w+1,	y-1);
			glVertex2f(x+w+1,	y+size+28+1);
			glVertex2f(x-1,		y+size+28+1);
			glVertex2f(x-1,		y-1);
		glEnd();
		
		color::apply(color::blue, state, color::colortype::bg);
		glBegin(GL_QUADS);
			glVertex2f(x,y);
			glVertex2f(x+w,y);
			glVertex2f(x+w,y+size+28);
			glVertex2f(x,y+size+28);
		glEnd();
		color::apply(color::blue, state, color::colortype::fg);
		if (state == focus && cursor < .4) {
			glBegin(GL_QUADS);
				float xt=x + _text_obj.width();
				glVertex2f(xt+11,y+12);
				glVertex2f(xt+19,y+12);
				glVertex2f(xt+19,y+size+14);
				glVertex2f(xt+11,y+size+14);
			glEnd();
		}
		_text_obj.render(x+10,y+16);

	}
	void Field::takeFocus() {
		parent -> focusOn(this);
		glfwEnable(GLFW_KEY_REPEAT);
		cursor=0;
	}
	void Field::setText(std::string _text) {
		text=_text;
		_text_obj.str=_text;
	}
	void Field::handle(const event& e) {
		if (e.type==ui::eventtype::click && e.click.state==ui::pressed) {
			takeFocus();
		} else if (e.type==ui::eventtype::text && e.key.state==ui::pressed) {
			if (!numeric || ((e.key.key >= '0' && e.key.key <= '9') || e.key.key=='.')) {
				text+=e.key.key;
				_text_obj.str=text;

				if (numeric) { /* stupidity filters */
					if (text=="00") setText("0");
					else if (text[0]=='0') setText(toString(stringTo<unsigned long long>(text)));
					// FIXME this is the PHP way of accomplishing this task. It needs to be significantly
					// optimized.
				}
			}
			cursor=.3;
			parent->callback(this);
		} else if (e.type==ui::eventtype::key && e.key.state==ui::pressed) {
			switch (e.key.key) {
				case GLFW_KEY_BACKSPACE:
					text=text.substr(0,text.size()-1);
					_text_obj.str=text;
					cursor=.3;
				break;
				case GLFW_KEY_ESC:
					text="";
					_text_obj.str="";
					cursor=.3;
				break;
				case GLFW_KEY_ENTER:
					parent->defocus();
					//parent -> focuswidget = nullptr;
				break;
			}
			parent->callback(this);
		}
	}
	void Field::onBlur() {
		glfwDisable(GLFW_KEY_REPEAT);
	}
	bool Field::inhitbox(float x, float y) {
		float px = abx(), py = aby();
		return (x>=px && x<=px+w) && (y>=py && y<=py+28+size);
	}
}