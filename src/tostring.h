#pragma once
#include <string>
#include <sstream>
template <typename T> std::string toString(T n) {
	std::ostringstream ss;
	ss<<n;
	return ss.str();
}
template <typename T> T stringTo(std::string n) {
	std::istringstream ss(n);
	T retval;
	ss >> retval;
	return retval;
}
bool stricmp(std::string& str1, std::string& str2);