#pragma once
#include <list>
#include "character.h"
#include "item.h"
#include "commodity.h"
#include "quantity.h"
#include "location_t.h"
namespace data {
	class system;
	class location : public locatable { public:
		std::list<character> characters;
		std::list<item> items;
		inventory<commodity*> commodities;
		// virtual ~location();
	};
}