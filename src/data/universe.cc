#include "universe.h"
#include <cstdlib>
#include <ctime>
#include <list>
#include "../tostring.h"
#include "../util.h"
#include "faction.h"
#include "../ptdist.h"
#include "map.h"
#include "condition.h"
namespace {
	char* makeName() {
		char* name;
		unsigned int namelen = rand()%7+5;
		name=new char[namelen];
		name[namelen]=0;
		for(int j=0; j<namelen; j++) name[j]='a'+rand()%('z'-'a');
		name[0]-=0x20;	// set first letter uppercase
		return name;
	}
}
namespace data {
	system*			systems;
	unsigned int	systemc;
	dist_t			usize;
	utime_t			utime;
	character*		player;
	const tech_t	maxtech = 20;
	std::list<item>	items;
	static inline float rand_pos(const int max, const int subt) {
//		if (max==0) TODO fix "illegal instruction" by handling 0 as special case
		return (float)((rand()%max)-subt) / 10.0f;
	}
	void generateUniverse(std::string shipname) {
		srand(time(NULL));
		utime=0;
		usize	= (float)(rand()%80+20) / 10.0f;
		systemc = (rand()%40 + 40) * usize;
		systems = new system[systemc];
		struct facdata {float x,y,r; Faction* fac;} fac_inf[factionc];
		const int maxdist = usize * 20, subt = usize * 10;
		for(int i=0; i<factionc; i++) {
			fac_inf[i].x=rand_pos(maxdist,subt);
			fac_inf[i].y=rand_pos(maxdist,subt);
			fac_inf[i].r=rand_pos(maxdist-40,-40);	// 4.0f minimum size
			fac_inf[i].fac=&factions[i];	//hacky
			$("%: %",factions[i].name, factions[i].makename());
		}
		for(int i=0; i<systemc; i++) {
			systems[i].x=rand_pos(maxdist,subt);
			systems[i].y=rand_pos(maxdist,subt);
			facdata* fac = nullptr;
			for (auto& f : fac_inf) {
				if (ptdist(f.x,f.y, systems[i].x,systems[i].y) <= f.r) {
					if ((fac==nullptr || fac->r > f.r) && rand()%30!=0) {		// smaller factions get some priority, so they don't get swallowed by the big ones.
						fac=&f;
					}
				}
			}
			systems[i].faction = (fac==nullptr ? nullptr : fac->fac);
			systems[i].marketcontrol = fac!=nullptr ? 
			/* faction assigned */		std::max((float)(rand()%10)/10.0f, systems[i].faction->marketcontrol) :
			/* independent system */	(float)(rand()%10)/10.0f;
			
			if (fac==nullptr) {
				systems[i].name="IPRIS "+toString(i);
			} else {
				systems[i].name=systems[i].faction->makename(1);
			}
			systems[i].size=(float)(rand()%20+1) / 15.f;
			systems[i].techlevel= fac!=nullptr ? 
			/* faction assigned */		(rand()%std::min(systems[i].faction->techlimit, maxtech)) :
			/* independent system */	(rand()%maxtech);

			

			// add planets
			float sz = (systems[i].size*15);
			float base=sz*sz;
			float cursor=base;
			std::list<planet> planets;
			while (cursor<base*4) {
				planet p;
				cursor+=(float)(rand()%50+10)/10.0f;
				if (fac==nullptr) {
					p.name=systems[i].name+' '+toString(i);
				} else {
					p.name=systems[i].faction->makename(1);
				}
				p.orbit=cursor;
				
				if (p.orbit-base<50*systems[i].size) { // we'll pretend this is the habitable zone
					p.type=planet::Type::mercurial;
				} else if (p.orbit-base>150*systems[i].size) { // gas giant line
					p.type=planet::Type::gasgiant;				
				} else {
					p.type=(planet::Type)(rand()%((int)planet::Type::barren-(int)planet::Type::oceanic)+(int)planet::Type::oceanic); // I hate this line so very much.
				}
				
				p.size=(p.orbit/20) + (float)(rand()%100-50)/100.0f;
				if (p.type==planet::Type::gasgiant) p.size*=2.1;
				cursor+=p.size*5;
				
				if (systems[i].techlevel==0) {
					p.techlevel=0;
				} else {
					p.techlevel=rand()%systems[i].techlevel;
				}
				planets.push_back(p);
			}
			systems[i].planetc=planets.size();
			systems[i].planets=new planet[systems[i].planetc];
			size_t j = 0;
			for (planet p : planets) {;
				systems[i].planets[j] = p;
				j++;
			}
			for (int j = 0; j<commodityc; j++) {
				if (data::commodities[j].tradableIn(&systems[i]) && rand()%11 > 3)
					systems[i].products.push_back(&data::commodities[j]);
			}
			// initialize commodities
			systems[i].resetMarket();	
		}
		
		system* start_sys;
		do {	// brute-forcefully make sure the player lands somewhere with a faction
			start_sys = &systems[rand()%systemc];
		} while (start_sys->faction==nullptr);
		
		//entry-level ship
		ship* ps = new ship(&start_sys->faction->shipclasses[start_sys->faction->shipclassc-1], shipname);
		ps->loc = {location_t::Type::system, start_sys};
		player->loc = {location_t::Type::ship, ps};
		player->money=1000;
	}

	void tick(utime_t t) {
		for (utime_t i = 0; i<t; i++) { // TODO this algorithm could probably be factored out so it doesn't need a loop
			// TODO handle galactic events, journal subscriptions, etc.
			for (size_t s = 0; s<systemc; s++) {
				//$("\x1b[31;1mSYSTEM %\x1b[0m", systems[s].name);
				for (std::list<data::condition>::iterator i = systems[s].conditions.begin(); i!=systems[s].conditions.end(); i++) {
					if (i->timeleft==0) {
						i = systems[s].conditions.erase(i);
					} else { // the condition disappears *the day after* the timer hits zero. why? because.
						--i->timeleft;
					}
				}
				for (int c = 0; c < data::conditions::count; c++) {
					if (!systems[s].habitable()) continue;
					condition::prototype& cp = *data::conditions::all[c];
					utime_t afftime = (*cp.affect_det)(&systems[s], cp.affect_det_param);
					if (afftime == 0) continue;
					
					unsigned int chance = rand()%((int)(cp.probability))*2+1;
					//$("\tRolling dice for %: rolled a %", data::conditions::all[c]->name, (int)chance);
					if (chance == 1 && !systems[s].afflictedBy(&cp))
						systems[s].conditions.push_back(data::condition{
							/*      prototype */ &cp,
							/* time remaining */ afftime,
							/*        operand */ cp.operand_det!=nullptr ? (*cp.operand_det)(&systems[s]) : nullptr
					 	});
						//$("Afflicting system \x1b[33;1m%\x1b[0m with % for %", systems[s].name, cp.name, afftime);
				}
				systems[s].marketTick();
			}
			utime++;
		}
	}

	void cleanupUniverse() {
		for(int i=0; i<systemc; i++) {
			if (systems[i].planetc>0) delete[] systems[i].planets;
		}
		delete[] systems;
		systemc=0;
		items.clear();
	}
}