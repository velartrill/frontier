#include "ship.h"
namespace data {
	extern item	aegis_items[],
			empire_items[],
			society_items[];
	ship::Class aegis_shipclasses[] = {
		/*				hull	energy	cargo	size	weapons	utils	crew 	img*/
		{"Dreadnought",	950000,	50000,	5000,	2550,	75,		40,		750,	{"ship/aegis/dreadnought.tga",0,0}, 20, // asteroid-sized behemoth
			"The Dreadnought is the apex of Aegis engineering and scientific "
			"accomplishment. Over a kilometer long and packed with enough firepower "
			"to overthrow the Second Empire overnight, the Dreadnought is the "
			"incarnation of overkill.",nullptr},
		{"Conciliator",	12000,	2500,	1200,	120,	10,		7,		25,		{"ship/aegis/conciliator.tga",512,512},19,// warship
			"A top-of-the-line combat cruiser, the Conciliator is the Aegis' "
			"premier force-projection vehicle, providing heavy artillery in "
			"pitched space battles and more than able to wipe out all but the "
			"hardiest pirate fleet on its own.",nullptr},
		{"Piecemaker",	9000,	1300,	650,	40,		5,		6,		15,		{"ship/aegis/piecemaker.tga",512,512}, 18,// battlecruiser
			"While technically a mere rapid-assault craft, the Piecemaker's "
			"Aegis engineering puts its offensive capabilities well ahead of "
			"any competitors.",nullptr},
		{"Collaborator",2400,	1500,	15,	7,		0,		10,		1,		{"ship/aegis/collaborator.tga",0,0}, 15,// spy ship
			"Built originally to spy on enemy troop movements, the Collaborator "
			"has become popular among civilians for its advanced power distribution "
			"and telemetry synchronization capabilities, enabling it to run "
			"many more utilities than other ships of its size.",nullptr},
		{"Emissary",	6500,	950,	300,	24,		2,		4,		40,		{"ship/aegis/emissary.tga",512,512}, 13,// diplomatic transport
			"Mostly used for ferrying ambassadors around the galaxy, the "
			"Emissary strikes a balance between luxury and defensive capabilities. ",nullptr},
		{"Envoy",		4500,	500,	50,	15,		1,		2,		3,		{"ship/aegis/envoy.tga",512,512}, 11,// civvie ship
			"The standard design of civilian ships in the Aegis, built with "
			"luxury and speed in mind.",&aegis_items[0]}
	};
	ship::Class empire_shipclasses[] = {
		{"Imperatrix",	5400,	1200,	560,	65,		12,		2,		77,		{"ship/empire/imperatrix.tga",512,512}, 15,
			"Designed by the Imperial Corps of Engineers and collaboratively manufactured by "
			"Mentex Megascale, Inferion Hyperworks, and Lidraxe Drive Systems, the Imperatrix "
			"is a warship built for heavy artillery support. An Imperatrix serves as the Empress's "
			"personal flagship, and the ship design serves as a symbol of Imperial unity.",nullptr},	// warship
		{"Bellator",	3400,	800,	240,	45,		7,		3,		45,		{"ship/empire/bellator.tga",0,0},11,"",nullptr},	// battlecruiser
		{"Ructatrix",	2500,	250,	50,	30,		3,		5,		38,		{"ship/empire/ructatrix.tga",512,256},8,"",nullptr},	// support craft
		{"Incitatus",	1200,	120,	75,	10,		0,		2,		4,		{"ship/empire/incitatus.tga",256,256},5,
			"Produced across the empire by Mentex Megascale Manufacturing Ltd., the Incitatus "
			"is an entry-level cargo hauler. As the sales materials boast, it usually doesn't "
			"explode.*\n \n*Guarantee not legally binding. Do not expose Incitatus to "
			"greater than 1.3 megaflenks of firepower. Do not taunt hyperdrive.",&empire_items[0]}	// merchant ship
	};
	ship::Class society_shipclasses[] = {
		{"AX-52 Evicerator",	7400,	1200,	650,	40,		10,		2,		35,		{"ship/society/ax52.tga",0,0},16,"",nullptr},	// warship
		{"AX-42 Suppressor",	6500,	800,	460,	35,		5,		3,		20,		{"ship/society/ax42.tga",0,0},13,"",nullptr},	// battlecruiser
		{"BL-29 Raptor",		3450,	250,	150,	20,		3,		5,		12,		{"ship/society/bl29.tga",0,0},10,"",nullptr},	// support craft
		{"XR-37 Oppressor",		2500,	750,	30,		6,		0,		7,		2,		{"ship/society/xr37.tga",0,0},8,"",nullptr},	// spy ship
		{"CR-50",				450,	50,		350,	60,		0,		4,		2,		{"ship/society/cr5x.tga",512,256},7,"",nullptr},		// cargo ship
		{"CR-58",				450,	50,		60,		60,		0,		6,		150,	{"ship/society/cr5x.tga",512,256},7,"",nullptr},	// transport ship
		{"CR-01",				200, 	90,		15,		4,		0,		2,		1,		{"ship/society/cr01.tga",512,256}, 6,
			"The CR-01 was designed as an emergency one-person transport craft for use in "
			"evacuation of personnel from beseiged worlds. Obsolete models are sold as "
			"military surplus.",&society_items[0]}		// personal transport ship
	};
	/*
	ship::Class ordolucri_shipclasses[] = {
		{"Kingpin",			5400,	1200,	2400,	65,		12,		2},
		{"Monopoly",		3400,	800,	760,	45,		7,		3},
		{"Executive",		3400,	800,	760,	45,		7,		3},
		{"Exchequer",		2500,	250,	400,	30,		3,		5},
		{"Appropriator",	1200, 	120,	650,	10,		0,		2},
		{"Competitor",		1200, 	120,	650,	10,		0,		2}
	}
	*/
}
