#pragma once
#include <string>
#include "location_t.h"
#include "types.h"
namespace data {
	struct pronoun_set {	//customize for non-English translations
		const char* nom, *acc, *gen, *pos, *obl, *ref;
	};
	class character : public locatable { public:
		std::string				name;
		balance_t				money;
		enum Pronoun {female=0, nonbinary=1, male=2}	pronoun;
		/* stats */
		
		static pronoun_set pronouns[];
		inline const pronoun_set* pron(bool first=false) {
			return &pronouns[pronoun + (2*first)];	// nope, I'm not kidding.
		}
		void spend(price_t);
		void earn(price_t);
	};
}