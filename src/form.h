#pragma once
#include "ui.h"
#include "ui/label.h"
#include "color.h"
#include <string>
#include <vector>
namespace ui {
	class Form : public Controller {
		std::vector<Label*> labels;
		public:
			float cursor;
			color::scheme* color;
			Form();
			virtual ~Form();
			void add_with_label(std::string label, Widget* w, float height = 90);
			void render();
			Label* addtitle(std::string text);
	};
}