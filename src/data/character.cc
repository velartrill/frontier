#include "character.h"
namespace data {
	pronoun_set character::pronouns[] = {
		{"she","her","her","hers","her","herself"},
		{"xe","hir","hirs","hirs","hir","hirself"},
		{"he","him","his","his","him","himself"},
		
		{"She","Her","Her","Hers","Her","Herself"},
		{"Xe","Hir","Hirs","Hirs","Hir","Hirself"},
		{"He","Him","His","His","Him","Himself"},
	};
	void character::spend(price_t amt) {
		// TODO add support for logging expenditures.
		money -= amt;
	}
	void character::earn(price_t amt) {
		// TODO add support for logging revenue.
		money += amt;
	}
}