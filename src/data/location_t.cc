#include "location.h"
#include "ship.h"
namespace data {
	system* locatable::getSystem() const {
		if (loc.type==location_t::Type::system) return (system*)loc.loc; else
			return loc.loc->getSystem();
		// possibility for segfault if called on a system. This may never arise, but
		// putting a FIXME here just in case
	}
	ship* locatable::getShip() const {
		if (loc.type==location_t::Type::ship) return (ship*)loc.loc; else
			return loc.loc->getShip();
		// all sorts of possibility for things to go wrong here if you're not careful
	}
}