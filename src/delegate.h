#pragma once

class func { public:
	virtual void operator()()=0;
};

template <class T> class funcmodel : public func {
	T* xthis;
	typedef void (T::*fptr)();
	void (T::*ptr)();
public:
	funcmodel(T* _this, fptr _ptr) {
		xthis=_this;
		ptr=_ptr;
	}
	void operator()() {
		(xthis->*ptr)();
	}
};