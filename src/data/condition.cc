#include "condition.h"
#include "map.h"
#define EV_MIN 10
namespace data {
	namespace {
		utime_t aff_unaligned(system* sys, unsigned int max) {
			if (sys->faction!=nullptr) return 0;
			return rand() % EV_MIN + (max-EV_MIN);
		}
		utime_t aff_consumerist(system* sys, unsigned int max) {
			if (sys->faction==nullptr || sys->faction->ideology==Faction::Ideology::consumerist)
				return rand() % EV_MIN + (max-EV_MIN);
			else return 0;
		}
		utime_t aff_noncommie(system* sys, unsigned int max) {
			if (sys->faction!=nullptr && sys->faction->ideology==Faction::Ideology::communist)
				return rand() % EV_MIN + (max-EV_MIN);
			else return 0;
		}
		utime_t aff_socialist(system* sys, unsigned int max) {
			if (sys->faction!=nullptr && sys->faction->ideology==Faction::Ideology::socialist)
				return rand() % EV_MIN + (max-EV_MIN);
			else return 0;	
		}
	}
	namespace conditions {
		condition::prototype
			war				=	{"War",				aff_unaligned,30,	nullptr, 165},
			anarchy			=	{"Anarchy",			aff_consumerist,14,	nullptr, 190},
			unrest			=	{"Unrest",			aff_noncommie,120,	nullptr, 150},
			drought			=	{"Drought",			aff_noncommie,15,	nullptr, 60},
			famine			=	{"Famine",			aff_consumerist,19,	nullptr, 60},

			shortages 		=	{"Shortages",		aff_socialist,20,	nullptr, 80},
			overpopulation	=	{"Overpopulation",	aff_socialist,60,	nullptr, 100},
			bureaucracy		=	{"Bureaucracy",		aff_socialist,17,	nullptr, 90},

			marketcollapse	=	{"Market collapse",	aff_consumerist,39,	nullptr, 160},
			bubble			=	{"Bubble economy",	aff_consumerist,20,	nullptr, 70};

		condition::prototype* all[] = {
			&war, &anarchy, &unrest, &drought,
			&famine, &shortages, &overpopulation, &bureaucracy,
			&marketcollapse, &bubble
		};
		const unsigned char count = sizeof(all) / sizeof(condition::prototype*);
	}
}