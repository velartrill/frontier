#include "imgbtn.h"
#include "../color.h"
#include "../circle.h"
#include "../window.h"
namespace ui {
	ImgBtn::ImgBtn(std::string _label, float _x, float _y, Image* _image) :
		_label_obj(_label){
		label	=	_label;
		image	=	_image;
		x		=	_x;
		y		=	_y;
		w		=	image->w;
		h		=	image->h;
		signal	=	nullptr;
	}
	void ImgBtn::render() {
		float x = abx(), y=aby(); //cache the results so we don't have to keep calling a recursive function
							// it's *possible* the compiler knows better with a const function, but I'm
							// not betting on it.
		if (state==hover) {
			glEnable(GL_BLEND);
			color::apply(color::purple, off, color::colortype::fg,.5);
			draw::circleGrad(x+w/2,y+h/2,std::max(w,h)*.75,{1,.8,1},1,{.4,.2,.8},0);
			float sx = x, sw=_label_obj.width(), sy;
			if (y<window::height - 95)
				sy=y + (h+5);
			else
				sy=y - 10;
			color::apply(color::purple, off, color::colortype::border);
			glEnable(GL_MULTISAMPLE);
				glBegin(GL_LINE_STRIP);
					glVertex2f(sx-10,sy+26);
					glVertex2f(sx+10+sw,sy+26);
					glVertex2f(sx+10+sw,sy-8);
					glVertex2f(sx-10,sy-8);
					glVertex2f(sx-10,sy+27);
				glEnd();
			glDisable(GL_MULTISAMPLE);
				color::apply(color::purple, off, color::colortype::bg, .3);
				glBegin(GL_QUADS);
					glVertex2f(sx-10,sy+26);
					glVertex2f(sx+10+sw,sy+26);
				color::apply(color::purple, off, color::colortype::bg,.6);
					glVertex2f(sx+10+sw,sy-8);
					glVertex2f(sx-10,sy-8);
				glEnd();
			color::apply(color::purple, off, color::colortype::fg);
			_label_obj.render(sx, sy);

			glDisable(GL_BLEND);
			glColor3f(1,1,1);
			image->render(x,y);
		} else if(state==active) {
			glEnable(GL_BLEND);
			color::apply(color::purple, off, color::colortype::fg,.5);
			draw::circleGrad(x+w/2,y+h/2,std::max(w,h)*.65,{.9,.7,1},.8,{.4,.2,.8},0);
			glDisable(GL_BLEND);
			image->tint=true;
			glColor3f(.8,.6,1);
			image->render(x+(w*.15),y+(h*.15),w*.75,h*.75);
		} else {
			glColor3f(1,1,1);
			image->render(x,y);
		}

	}
	void ImgBtn::handle(const ui::event& e) {
		if (e.type==ui::eventtype::click && e.click.state==ui::released && parent->hoverwidget==this) {
			// parent reference is of... questionable correctness.
			if (e.click.btn==ui::event_click::right) {
				parent->auxcallback(signal==nullptr ? this : signal);
			} else {
				parent->callback(signal==nullptr ? this : signal);
			}
		}
	}
	bool ImgBtn::inhitbox(float px, float py) {
		float x = abx(), y = aby();
		return (px>=x && px<=x+w && py>=y && py<=y+h);
	}
	void ImgBtn::loop(){};
}