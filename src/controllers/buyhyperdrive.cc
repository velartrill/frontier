#include "buyhyperdrive.h"
#include "../window.h"
#include "../util.h"
#include "../strings.h"
#include "../tostring.h"
#include "../alert.h"
namespace Controllers {
	namespace {
		inline data::price_t hyperdrive_price(data::item* hyperdrive) {
			data::price_t curDrivePrice = data::player->getShip()->hyperdrive->price;
			data::price_t newDrivePrice = hyperdrive->price;
			return newDrivePrice - (curDrivePrice * .8);
		}
	}
	BuyHyperdrive::BuyHyperdrive() :
		l_title("Hyperdrives",5,window::height-45,300,40,{1,1,1}),
		l_header("",0,window::height-60,window::width-400,40,{0,0,0}),
		l_desc("",3,window::height-90,window::width-400,15,{0,0,0}),
		b_buy("Buy Drive",window::width-540,10,190,50,&color::green),
		t_stats(0,250,window::width-420,100,200)
	{
		l_title.text.align(Font::align::center);
		add(&l_title);
		data::system* s = data::player->getSystem();
		data::Faction* f = s->faction;
		factionColor=f->color;
		float cursor = 112;
		for (int i = 0; i<f->itemc; i++) {
			if (f->items[i].type==data::item::Type::hyperdrive && f->items[i].techlevel <= s->techlevel) {
				auto b = new ui::Button(f->items[i].name, 0, window::height-cursor, 299, 50, factionColor);
				b->signal=new DriveSignalObject{b, f->items+i};
				b->label.align(Font::align::left);
				driveList.add(b);
				cursor+=52;
			}
		}

		driveList.x=10,		driveList.y=0;
		driveList.w=300,	driveList.h=window::height;
		add(&driveList);
		
		driveView.add(&b_buy);
		driveView.add(&l_header);
		driveView.add(&l_desc);
		t_stats.addrow("Range",			"",		{0,0,0});
		t_stats.addrow("Speed",			"",		{0,0,0});
		t_stats.addrow("Efficiency",	"",		{0,0,0});
		t_stats.addrow("Tech Level",	"",		{0,0,0});
		t_stats.addrow("Price",			"",		{0,0,0});
		r_range	= &t_stats.rows[0];
		r_speed	= &t_stats.rows[1];
		r_efficiency = &t_stats.rows[2];
		r_techlevel	= &t_stats.rows[3];
		r_price	= &t_stats.rows[4]; 			//eww
				
		driveView.add(&t_stats);
		driveView.x=340,				driveView.y=0;
		driveView.w=window::width-320,	driveView.h=window::height;
		selection=NULL;
	}
	BuyHyperdrive::~BuyHyperdrive() {
		for (Widget* w : driveList.widgets) {
			delete (DriveSignalObject*)(((ui::Button*)w) -> signal); // oh god
			delete w; // let's *try* not to leak memory like a sieve
		}
	}
	void BuyHyperdrive::callback(void* w) {
		if (hoverwidget==&driveList) {
			if (selection==NULL) add(&driveView);
				else selection->b->toggle=ui::Button::togglestate::none;
			auto so = (DriveSignalObject*)w;
			selection=so;
			l_header.text.str=so->i->name;
			l_desc.text.str=so->i->desc;
			if (selection->i==data::player->getShip()->hyperdrive) {
				r_price->namecolor={.4,.4,.4};
				r_price->textcolor={.4,.4,.4};
				r_price->text="N/A";
			} else {
				if (hyperdrive_price(so->i) < 0)
					r_price->namecolor={0,.8,0},
					r_price->textcolor={0,.8,0};
				else if (hyperdrive_price(so->i) > data::player->money)
					r_price->namecolor={.8,0,0},
					r_price->textcolor={.8,0,0};
				else
					r_price->namecolor={0,0,0},
					r_price->textcolor={0,0,0};
				r_price->text=strings::price(hyperdrive_price(so->i));
			}
			r_range->text=strings::distance(so->i->range);
			r_speed->text=strings::distance(so->i->speed)+" per day";
			r_efficiency->text=strings::hyperdrive_efficiency(so->i->rate);
			r_techlevel->text=strings::techlevel(so->i->techlevel);
			so->b->toggle=ui::Button::togglestate::on;
		} else {
			if (w==&b_buy) {
				if (selection->i==data::player->getShip()->hyperdrive) {
					window::alert("You already have a hyperdrive of that model.",&color::red);
				} else if (hyperdrive_price(selection->i) < 0) {
					window::alert(std::string("Old drive sold for ")+strings::price(-hyperdrive_price(selection->i))+"!", &color::green);
					data::player->spend(hyperdrive_price(selection->i));
					data::player->getShip()->hyperdrive=selection->i;
				} else if (hyperdrive_price(selection->i) <= data::player->money) {
					window::alert(std::string("New drive purchased for ")+strings::price(hyperdrive_price(selection->i))+"!", &color::green);
					data::player->spend(hyperdrive_price(selection->i));
					data::player->getShip()->hyperdrive=selection->i;
				} else
					window::alert(std::string("You need an additional ")+strings::price(hyperdrive_price(selection->i)-data::player->money)+" to afford this drive.",&color::red);
			}
		}
	}
	void BuyHyperdrive::handle(const ui::event& e) {
		if (e.type==ui::eventtype::key && e.key.key==GLFW_KEY_ESC) window::back();
		else Controller::handle(e);
	}
	void BuyHyperdrive::render() {
		if (selection!=NULL) {
			glColor3f(factionColor->active.fg.r+.5,factionColor->active.fg.g+.5,factionColor->active.fg.b+.5);
			glBegin(GL_QUADS);
				glVertex2f(driveList.x+driveList.w,0);
				glVertex2f(driveList.x+driveList.w,window::height);
				glVertex2f(window::width,window::height);
				glVertex2f(window::width,0);
			glEnd();
		}
		
		glEnable(GL_BLEND);
		
		glBegin(GL_QUADS);
			glColor4f(0,0,0,.2);
				glVertex2f(driveList.x+driveList.w,0);
				glVertex2f(driveList.x+driveList.w,window::height);
			glColor4f(0,0,0,0);
				glVertex2f(driveList.x+driveList.w+30,window::height);
				glVertex2f(driveList.x+driveList.w+30,0);
		glEnd();
		
		glBegin(GL_QUADS);
			color::apply(*factionColor,ui::Widget::off,color::colortype::bg,.5);
				glVertex2f(driveList.x, window::height);
				glVertex2f(driveList.x+driveList.w, window::height);
			color::apply(*factionColor,ui::Widget::off,color::colortype::bg,.3);
				glVertex2f(driveList.x+driveList.w, 0);
				glVertex2f(driveList.x, 0);
		glEnd();
			color::apply(*factionColor,ui::Widget::active,color::colortype::fg);
		glBegin(GL_LINES);
			glVertex2f(driveList.x, 0);
			glVertex2f(driveList.x, window::height);
			
			glVertex2f(driveList.x+driveList.w,0);
			glVertex2f(driveList.x+driveList.w,window::height);
		glEnd();
		glDisable(GL_BLEND);
		Controller::render();
	}
}