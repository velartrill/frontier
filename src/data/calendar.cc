#include "calendar.h"
namespace data {
	namespace calendar {
		const day_t	days_in_month = 40;
		const char* days[] = {"Yarpday", "Spuckday", "Morzday", "Gromday","Yorthday","Quopday"};
		const day_t	days_in_week = sizeof(days)/sizeof(days[0]);
		const char* months[] = {"Burglary","Blunder","Gluttony","Bureaucracy","Lust","Wrath","Sloth",
							"Envy","Greed","Pride","The Aftermath"};
		const month_t months_in_year = sizeof(months)/sizeof(months[0]);
		const utime_t days_in_year = days_in_month * months_in_year;
		month_t month(utime_t timestamp) {
			return (timestamp % days_in_year) / days_in_month;
		}
		year_t year(utime_t timestamp) {
			return timestamp / days_in_year + 11257; // dates have got to be appropriately futuristic, after all
		}
		const char* weekday(utime_t timestamp) {
			return days[timestamp % days_in_week];
		}
		day_t day(utime_t timestamp) {
			return timestamp % days_in_month + 1; // there's no zeroth day of Burglary
		}
	}
}
