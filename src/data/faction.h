#pragma once
#include "../color.h"
#include <string>
#include "ship.h"
namespace data {
	struct Faction {
		const char* name, *desc;
		color::scheme* color;
		const char* img;
		size_t shipclassc;
		ship::Class* shipclasses;
		size_t itemc;
		data::item* items;
		enum class Ideology {consumerist, socialist, communist} ideology; // affects type of goods available
		tech_t techlimit; // members cannot be above this tech level
		float marketcontrol; // 1 = no weapons can be sold; 0 = get a nuclear warhead free with your methamphetamine salad
		struct {
			size_t syllablec;
			const char** syllables;
			size_t suffixc;
			const char** suffixes;
			bool longvowels, vowelclusters, consonantclusters, gemination;
			unsigned char minlen, maxlen, namec;
		} language;
		std::string makename(unsigned char names=0);
	};
	extern Faction factions[];
	extern const unsigned char factionc;
}