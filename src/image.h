//
// Image
//
// Provides a handle to TGA images through GLFW and OpenGL.
//
#pragma once
class Image { public:
	unsigned int /* GLuint */ _handle; /* because of inherited-from-C idiotic import "semantics,"
	                                    we'd have to import the entire damn GL namespace
                                        just to get one damn typedef. not worth polluting
	                                    the whole namespace, so we're pretending GLuint is
	                                    exactly the same on every architecture.
		                                Portability? What portability? */
	float w, h;                        
	Image(const char* name, float w, float h, bool tint = false);
	~Image();
	bool tint;
	void render(float x, float y);
	void render(float x, float y, float w, float h);
};