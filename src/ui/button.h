#pragma once
#include <string>
#include "../text.h"
#include "../ui.h"
#include "../delegate.h"
#include "../color.h"
namespace ui {

class Button : public Widget { public:
	TextBox label;
	float w, h;
	typedef Button* Group;
	void* signal;
	enum class togglestate { none, toggle, on } toggle;
	Button();
	Button(std::string, float _w, float _h=80);
	Button(std::string, float x, float y, float w, float h=80, color::scheme* btncolor = &color::purple);
	void render();
	void settext(std::string str);
	void loop();
	void handle(const event&);
	bool inhitbox(float x, float y);
	
	color::scheme* btncolor;
	Group* group;
};

}