#include "merchant.h"
#include "../window.h"
#include "../data/universe.h"
#include "../color.h"
#include "../tostring.h"
#include "../strings.h"
#include "../alert.h"
namespace Controllers {
	Merchant::Merchant(data::system* _sys) :
		quantitydialog		(this),
		cargo				(						40,							15,					window::width-80,	40),
		b_cancel			("Cancel Transaction",	(window::width-500)/2,		window::height-60,	230,				50,	&color::red),
		b_finalize			("",					(window::width-500)/2+250,	window::height-60,	230,				50, &color::green),
		b_modeselector_buy	("Buy",					(window::width-500)/2,		window::height-60,	230,				50,	&color::brown),
		b_modeselector_sell	("Sell",				(window::width-500)/2+250,	window::height-60,	230,				50, &color::brown),
		money				("",					0,							window::height-35,	window::width-15,	25, color::brown.base.fg)
	{	// establish the players
		sys=_sys;
		pship=data::player->getShip();
		// assign their roled
		buyFrom = sys;
		sellTo = pship;

		money.text.align(Font::align::right);
		money.text.str=strings::balance(data::player->money);
		add(&money);

		cargo.max=pship->shipclass->maxweight;
		// StatBar doesn't autosort, so we need to prepare for a bit of evil pointer trickery
		cargodata[0].name="Prospective cargo";
		cargodata[0].color=&color::blue;
		cargodata[1].name="Current cargo";
		cargodata[1].color=&color::brown;
		cargodata[2].name=cargodata[0].name;	//"Prospective cargo"; hey, why bother duplicating a string constant
												// when we can just give it a pointer? }:D
		cargodata[2].color=&color::brown;
		cargo.color=&color::purple;
		updateCargo();
		add(&cargo);

		g_scrollpane.w=window::width*.75;
		g_scrollpane.x=(window::width - g_scrollpane.w) / 2;
		g_scrollpane.y=window::height-100;
		g_scrollpane.h=55;
		g_scrollpane_dragging=false;
		g_scrollpane.parent=this;

		redrawScrollpane();

		quantitydialog_visible=false;
		
		b_finalize.visible=false;
		b_cancel.visible=false;
		add(&b_finalize);
		add(&b_cancel);
		b_modeselector=&b_modeselector_buy;
		b_modeselector_buy.group=&b_modeselector;
		b_modeselector_sell.group=&b_modeselector;
		add(&b_modeselector_sell);
		add(&b_modeselector_buy);

		/* queue flags */
		needsToRedraw=false;
		needsToClearHoverwidget=false;
	}
	void Merchant::redrawScrollpane() {	// a depressingly brute-force way of doing this, but it'll have to do.
		float cursor=-15;
		/* add rows */
			for(data::inventory<data::commodity*>::quantity& c : buyFrom->commodities.items) {
				commodities.push_back(
					new comm_grp(
						this, c, cursor, // self-explanatory
						std::string(c.ct->name), // label
						g_scrollpane, // parent
						c.ct->tradableIn(sys) // red button if it's illegal
					)
				);
				//using pointers here to cheat and avoid dealing with temporaries.
				//emplace breaks compatibility with OSX
				cursor-=45; g_scrollpane.h+=45;
			}
		/* end add rows */
		updateCommodities();
	}
	void Merchant::clearComGroups() {
		g_scrollpane.hoverwidget=nullptr;
		g_scrollpane.activewidget=nullptr;
		g_scrollpane.widgets.clear();
		for (comm_grp* c : commodities) delete c;
		commodities.clear();
	}
	Merchant::~Merchant() {
		clearComGroups();
	}
	Merchant::comm_grp::comm_grp (
		Merchant* m,
		data::inventory<data::commodity*>::quantity& c,
		float y,
		std::string name,
		ui::Group& scrollpane,
		bool sellable
	):
		button	("",	260,y-12,				scrollpane.w/3-20,	35),
		label	(name,	0,						y,					250-10),
		price	("",	button.w+button.x+10,	y,					(scrollpane.w/3)/2 - button.x+button.w, 20.0f, color::brown.base.fg),
		origprice("",	price.x+price.w,		y,					price.w, 20.0f, color::blue.base.fg)
	{
		commodity=c.ct;
		button.signal=this;
		button.label.align(Font::align::left);
		label.text.align(Font::align::right);
		price.shadow=true;
		price.shadowcolor=color::brown.active.bg;
		price.text.align(Font::align::right);
		if (m->buyFrom == m->pship) {
			origprice.text.str = strings::price(c.origprice);
			origprice.shadowcolor=color::blue.active.bg;
			origprice.text.align(Font::align::right);
			m->g_scrollpane.add(&origprice);
			if (sellable) {
				if (c.origprice < c.ct->priceIn(m->sys)) price.color=color::green.base.fg; else
				if (c.origprice > c.ct->priceIn(m->sys)) price.color=color::red.base.fg;
			} else goto dontAddPrice;
		}
		m->g_scrollpane.add(&price);
	dontAddPrice:
		m->g_scrollpane.add(&button);
		m->g_scrollpane.add(&label);
	}
	void Merchant::updateCommodities() {
		for(comm_grp* c : commodities) {
			if (buyFrom->commodities.count(c->commodity)==0) needsToRedraw=true;
			c->price.text.str=strings::price(c->commodity->priceIn(sys));
			data::quantity_t	totalHad = buyFrom->commodities.count(c->commodity),
								inTrade = curtrade.count(c->commodity);
			c->button.label.str=toString(totalHad - inTrade);
			if (c->commodity->tradableIn(sys)) {
				if (inTrade!=0) c->button.btncolor=&color::brown; else c->button.btncolor=&color::purple;
			} else {
				c->button.btncolor=&color::red;
			}
			// TODO update coloring
		}
	}
	data::weight_t Merchant::tradeWeight() {
		data::weight_t trade_weight = 0;
		for (data::inventory<data::commodity*>::quantity& q : curtrade.items) {
			trade_weight+=q.ct->weight * q.q;
		}
		if (buyFrom == sys)
			return trade_weight;
		else
			return -trade_weight;
	}
	void Merchant::updateCargo() {
		// how much does the current order weigh?
		data::weight_t trade_weight = tradeWeight();

		cargodata[0].val=0;
		cargodata[1].val=pship->weight();
		cargodata[2].val=0;
		// the prophesied pointer trickery
		if (trade_weight==0) {
			cargo.seriesc=1;
			cargo.series=cargodata+1;
			cargodata[1].color=&color::brown;
		} else if (trade_weight+pship->weight() > pship->weight()) { //post-trade weight > shipw
			cargo.seriesc=2;
			cargo.series=cargodata;
			cargodata[0].val=trade_weight+pship->weight();
			cargodata[1].color=&color::brown;
		} else if (trade_weight+pship->weight() < pship->weight()) { // weight < shipw
			cargo.seriesc=2;
			cargo.series=cargodata+1;
			cargodata[2].val=trade_weight+pship->weight();
			cargodata[1].color=&color::red;
		}
	}
	void Merchant::loop() {
		if (needsToClearHoverwidget) {
			if (hoverwidget) {
				hoverwidget->state=off;
				hoverwidget=nullptr;
			}
			needsToClearHoverwidget=false;
		}
		if (quantitydialog_visible)
			quantitydialog.loop();
		else if (needsToRedraw)
			clearComGroups(),
			redrawScrollpane(),
			needsToRedraw=false;
	}
	void Merchant::handle(const ui::event& e) {
		static bool scrollpane_active = false; // nasty hack to make de-highlighting of scrollpane more efficient
		if (quantitydialog_visible) { quantitydialog.handle(e); return; }
		/* otherwise */
		if (g_scrollpane_dragging) {
			if (e.type==ui::eventtype::motion) {
				auto prpos = g_scrollpane.y+e.motion.dy;
				if (prpos >= window::height-100)
					g_scrollpane.y=prpos;
			} else if (e.type==ui::eventtype::click && e.click.state==ui::released)
				g_scrollpane_dragging=false;
		} else {
			if (e.type==ui::eventtype::key && e.key.key==GLFW_KEY_ESC && e.key.state==ui::pressed) {
				window::back();
				return; // we're killing the scene, so no reason to bother finishing the circuit
			} else if (window::mousey > 125 && window::mousey < window::height-60) {
				if (!scrollpane_active) {
					if (hoverwidget) hoverwidget->state=off, hoverwidget=nullptr;
					scrollpane_active=true;
				}
				if (g_scrollpane.hoverwidget==nullptr && e.type==ui::eventtype::click && e.click.state==ui::pressed) {
					g_scrollpane_dragging=true;
				} else if (e.type==ui::eventtype::scroll) { // handle scroll scroll increase/decrease of quantity
					if (g_scrollpane.hoverwidget!=nullptr) {
						auto commodity = ((comm_grp*)((ui::Button*)g_scrollpane.hoverwidget)->signal)->commodity; // evil pointer trickery to extract what the widget refers back to
						if (!commodity->tradableIn(sys)) return;
#ifdef OS_MAC			// the closest we can get to supporting multitouch with GLFW.
	#define STEP 1
	#define TRADE_FACTOR -1  // reverse "natural scrolling"
#else
	#define STEP 20
	#define TRADE_FACTOR 1
#endif
						data::quantity_t curnum = curtrade.count(commodity);
						if (e.scroll.delta * TRADE_FACTOR > 0) {
							if (e.scroll.delta * TRADE_FACTOR <= getMax(commodity))
								modTrade(commodity, e.scroll.delta * TRADE_FACTOR);
						} else {
							if (curnum >= -e.scroll.delta * TRADE_FACTOR)
								modTrade(commodity, e.scroll.delta * TRADE_FACTOR);
						}
					} else { // let the user scroll through commodities with the scroll wheel.

						if (g_scrollpane.y-e.scroll.delta*STEP >= window::height-100)
							g_scrollpane.y-=e.scroll.delta*STEP;
#undef STEP
#undef TRADE_FACTOR
					}
				} else {
					g_scrollpane.handle(e);
				}
			} else {
				// reimplementing part of the UI framework here to keep the trade UI from being more inconsistent
				// than an elected Republican; cthulthu on a hoverbike but this is awful. next time I write a game,
				// scrollpanes are going to be part of the base UI library. #lessonlearned #lolhashtagsinc++amirite
				if (scrollpane_active) {
					scrollpane_active=false;
					if (g_scrollpane.hoverwidget)
						g_scrollpane.hoverwidget->state=(g_scrollpane.activewidget==g_scrollpane.hoverwidget?hover:off),
						g_scrollpane.hoverwidget=nullptr;
				} else {
					if (g_scrollpane.activewidget && e.type==ui::eventtype::click && e.click.state==ui::released) {
						g_scrollpane.activewidget->state=off;
						g_scrollpane.activewidget=nullptr;
					}
				}
				if (!g_scrollpane.activewidget) Controller::handle(e); // so that dragging off a button works right
			}
		}
	}
	void Merchant::modTrade(data::commodity* c, data::quantity_t q) {
			if (q>0) {
				data::price_t p;
				if (buyFrom==sys) p=c->priceIn(sys);
				else p=buyFrom->commodities[c].origprice;
				curtrade.add(c,q,p);
			} else
				curtrade.remove(c,-q); // note to self: remove's argument is supposed to be *positive*.
			updateCargo();
			updateCommodities();
			if (curtrade.items.empty()) finishTrade(); else {
				b_modeselector_sell.visible=false;
				b_modeselector_buy.visible=false;
				b_cancel.visible=true;
				b_finalize.visible=true;
				b_finalize.label.str="Finalize ("+strings::price(curtrade.totalPrice(sys))+")";
			}
	}
	void Merchant::finishTrade() {
			b_cancel.visible=false;
			b_finalize.visible=false;
			b_modeselector_sell.visible=true;
			b_modeselector_buy.visible=true;
			needsToClearHoverwidget = true;
			money.text.str=strings::balance(data::player->money);
	}
	void Merchant::auxcallback(void* w) {
		if (window::mousey > 125 && window::mousey < window::height-60){
			comm_grp* q = (comm_grp*)w;
			modTrade(q->commodity,getMax(q->commodity));
		}
	}
	data::quantity_t Merchant::getMax(data::commodity* commodity) {
		if (buyFrom==pship) return buyFrom->commodities.count(commodity) - curtrade.count(commodity);
		/* otherwise */
		data::weight_t curShipWeight = pship->weight() + tradeWeight();
		data::quantity_t byweight = (pship->shipclass->maxweight - curShipWeight) / commodity->weight;
		data::quantity_t byprice = (data::player->money-curtrade.totalPrice(sys)) / commodity->priceIn(sys);
		return std::min(std::min(byweight,byprice),buyFrom->commodities.count(commodity)-curtrade.count(commodity));
	}
	void Merchant::callback(void* w) {
		if (window::mousey > 125 && window::mousey < window::height-60){
			comm_grp* q = (comm_grp*)w;
			if (!q->commodity->tradableIn(sys)) {
				window::alert("You can't trade that in this system.",&color::red);
				return;
			}
			quantitydialog.commodity=q->commodity;
			if (g_scrollpane.hoverwidget != nullptr) { // this bug again...
				g_scrollpane.hoverwidget->state=ui::Widget::off;
				g_scrollpane.hoverwidget=nullptr;
			}
			quantitydialog.update();
			quantitydialog_visible=true;
		} else if (w==&b_finalize) {
			sellTo->commodities.transact(curtrade);
			buyFrom->commodities.reverseTransact(curtrade);
			data::price_t p = curtrade.totalPrice(sys);
			if (sellTo==pship)	data::player->spend(p), window::alert("Trade finalized; spent "+strings::price(p),	&color::green);
			else				data::player->earn(p),	window::alert("Trade finalized. Revenue "+strings::price(p)+"; profit "+strings::price(p - curtrade.totalOrigPrice()),	&color::green);
			curtrade.items.clear();
			updateCargo();
			updateCommodities();
			finishTrade();
		} else if (w==&b_cancel) {
			curtrade.items.clear();
			updateCargo();
			updateCommodities();
			finishTrade();
		} else if (w==&b_modeselector_buy && b_finalize.visible==false) { //haaack, "fixes" rare bug where mode is changed mid-trade
			buyFrom=sys;
			sellTo=pship;
			g_scrollpane.y=window::height-100;
			needsToRedraw=true;
		} else if (w==&b_modeselector_sell && b_finalize.visible==false) {
			buyFrom=pship;
			sellTo=sys;
			needsToRedraw=true;
			g_scrollpane.y=window::height-100;
		}
	}
	void Merchant::render() {
		const static float grtop = 90, grmid = 70;
		glBegin(GL_QUADS);
			glColor3f(0,0,.1);
				glVertex2f(0,grmid);
				glVertex2f(window::width,grmid);
			glColor3f(0,0,.05);
				glVertex2f(window::width,window::height-grmid);
				glVertex2f(0,window::height-grmid);
		glEnd();
		g_scrollpane.render();
		glEnable(GL_BLEND);
		glBegin(GL_QUADS);
			// bottom of screen
			
			/* shadow quad */
			glColor4f(.02,0,.05,0);
				glVertex2f(0,grtop);
				glVertex2f(window::width,grtop);
			glColor4f(.02,0,.05,.8);
				glVertex2f(window::width,grmid);
				glVertex2f(0,grmid);
			/* solid quad */
			glColor4f(.05,0,.1,1);
				glVertex2f(0,0);
				glVertex2f(window::width,0);
				glVertex2f(window::width,grmid);
				glVertex2f(0,grmid);

			// top of screen

			/* shadow quad */
			glColor4f(.06,0,.03,0);
				glVertex2f(0,window::height-grtop);
				glVertex2f(window::width,window::height-grtop);
			glColor4f(.06,0,.03,.8);
				glVertex2f(window::width,window::height-grmid);
				glVertex2f(0,window::height-grmid);
			glDisable(GL_BLEND);
			/* solid quad */
				glColor4f(.2,0,.1,1);
				glVertex2f(0,window::height);	
				glVertex2f(window::width,window::height);
				glVertex2f(window::width,window::height-grmid);
				glVertex2f(0,window::height-grmid);
		glEnd();
		if (b_finalize.visible) b_finalize.render(), b_cancel.render();
		else b_modeselector_buy.render(), b_modeselector_sell.render();
		cargo.render();
		if (quantitydialog_visible) quantitydialog.render();
		money.render();
	}
}