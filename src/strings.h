#include <string>
#include "data/types.h"
#include "data/calendar.h"
namespace strings {
	extern std::string price(data::price_t);
	extern std::string balance(data::balance_t);
	extern std::string weight(data::weight_t);
	extern std::string date(data::utime_t);
	extern std::string timespan(data::utime_t);
	extern std::string percentage(float);
	extern std::string techlevel(data::tech_t);
	extern std::string distance(data::dist_t);
	extern std::string hyperdrive_efficiency(float);
}