#pragma once
#include "../ui.h"
#include "../color.h"
#include <string>
#include <vector>

// TABLE
// Displays text in a two-column table
namespace ui {
	class Table : public Widget { public:
		struct row {
			const char* name;
			color::color namecolor;
			std::string text;
			color::color textcolor;
		};
		color::scheme* color;
		std::vector<row> rows;
		float lcolw;
		float size; //font size
		float w, h;
		Table(float x, float y, float w, float h, float lcolw, float size = 18, color::scheme* color = &color::blue);
		void loop() override;
		void handle(const event&) override;
		void render() override;
		void addrow(const char* label, std::string text);
		void addrow(const char* label, std::string text, color::color color);
		void addrow(const char* label, std::string text, color::color lcolor, color::color vcolor);
	};
}