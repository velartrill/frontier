#pragma once
#ifndef RELEASE
#include <stdio.h>
#include <iostream>
inline void $(const char* f) {
	while(*f) putchar(*f), ++f;
	putchar('\n');
}
template <typename T, typename... V> void $(const char* f, T value, V... args) {
	while (*f) {
		if (*f!='%' || (f[1]=='%' && ++f)) putchar(*f++);
		else {
			std::cout<<value;
			$(f+1, args...);
			break;
		}
	}
}
#endif