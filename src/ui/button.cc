#include "button.h"
#include <glfw.h>
#include "../util.h"
using std::string;
namespace ui {
	Button::Button() : label("",200-12,Font::align::center,Font::face::zekton,21.0f) {
		w=200;
		btncolor=&color::purple;
		group=nullptr;
		toggle=togglestate::none;
		signal=nullptr;
	}
	Button::Button(string str,float _w, float _h) : label(str,_w-12,Font::align::center,Font::face::zekton,21.0f){
		w=_w;
		h=_h;
		btncolor=&color::purple;
		group=nullptr;
		toggle=togglestate::none;
		signal=nullptr;
	}
	Button::Button(string str, float _x, float _y, float _w, float _h, color::scheme* _btncolor):
		label(str,_w-12,Font::align::center,Font::face::zekton,21.0f)
	{
		x=_x, y=_y, w=_w, h=_h;
		btncolor=_btncolor;
		group=nullptr;
		toggle=togglestate::none;
		signal=nullptr;
	}
	void Button::render() {
		float x = abx();
		float y = aby();
		if ((group!=nullptr && *group == this) || toggle==togglestate::on)
			color::apply(*btncolor,toggle==togglestate::on?off:state, color::colortype::fg);
		else
			color::apply(*btncolor,toggle==togglestate::on?off:state, color::colortype::bg);
		
		glBegin(GL_QUADS);
			glVertex2f(x,y);
			glVertex2f(x+w,y);
			glVertex2f(x+w,y+h);
			glVertex2f(x,y+h);
		glEnd();

		if ((group!=nullptr && *group == this) || toggle==togglestate::on)
			color::apply(*btncolor,toggle==togglestate::on?off:state, color::colortype::bg);
		else
			color::apply(*btncolor,toggle==togglestate::on?off:state, color::colortype::fg);
		
		label.render(x+6,y+((h-21)*.5));
	}
	void Button::settext(string str) {
		label.str=str;
	}
	void Button::handle(const event& e) {
		if (e.type==ui::eventtype::click && e.click.state==ui::released && parent->hoverwidget==this) {
			// parent reference is of... questionable correctness.
			if (e.click.btn==ui::event_click::right) {
				parent->auxcallback(signal==nullptr ? this : signal);
			} else {
				if (group!=nullptr)						*group = this;
				else if (toggle==togglestate::toggle)	toggle=togglestate::on;
				else if (toggle==togglestate::on)		toggle=togglestate::toggle;
				parent->callback(signal==nullptr ? this : signal);
			}
		}
	}
	void Button::loop() {};
	bool Button::inhitbox(float px, float py) {
		return (px>=abx() && px<=abx()+w && py>=aby() && py<=aby()+h);
	}
}