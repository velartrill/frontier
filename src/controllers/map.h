#pragma once
#include "../ui.h"
#include "../image.h"
#include "../text.h"
#include "../ui/button.h"
#include "../ui/field.h"

namespace data { class system; }
namespace Controllers {
	class Map : public ui::Controller {
		void update();
	public:
		Map();
		void render()					override;
		void loop() 					override;
		void handle(const ui::event&)	override;
		void callback(void*)			override;
		void reactivate(void* v)		override;
		void show(ui::Group*);
		void hilite(data::system*);
		enum {galactic, local} mode;
		ui::Group* dialog;
		float distfactor;
		float sizefactor;
		float radius;
		void* hlt;
		data::system* cursys,
					* hilitesys;
		Text sysname, facname;
		TextBox date;
		PosText shipname, shipclass;
		ui::Button::Group	b_show;
		ui::Button				b_show_factions, b_show_informants, b_show_none,
							b_galactic_view,
							b_find_system;

		class FindDialog : public ui::Group {
			ui::Button search, cancel;
			ui::Field name;
			public: FindDialog();
			void callback(void*) override;
			void loop() override;
		};
		FindDialog find;
	};
}