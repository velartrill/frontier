#include "types.h"
namespace data {
	namespace calendar {
		extern const day_t	days_in_month;
		extern const month_t months_in_year;
		extern const char* months[], * days[];

		extern month_t		month(utime_t);
		extern const char*	weekday(utime_t);
		extern day_t		day(utime_t);
		extern year_t		year(utime_t);
	}
}