#pragma once
#include "../ui.h"
#include "../image.h"
#include "../text.h"
#include <string>
namespace ui {
	class ImgBtn : public Widget { public:
		float w, h;
		Image* image;
		Text _label_obj;
		void* signal;
		std::string label;
		ImgBtn(std::string label, float x, float y, Image*);
		void render() override;
		bool inhitbox(float x, float y) override;
		void loop() override; // because C++ won't take NULL for an answer
		void handle(const event&);
	};
}