#include "circle.h"
#include <cmath>
namespace draw {
	// adapted from http://www.opengl.org/discussion_boards/showthread.php/167955-drawing-a-smooth-circle
	void circle(float x, float y, float radius, float quality, bool filled){
		const float DEG2RAD = 3.14159/(22.5*quality);
		glBegin(filled ? GL_TRIANGLE_FAN : GL_LINE_LOOP);
			if(filled) glVertex2f(x,y);
			for (int i=0; i<(45*quality)+(filled? 1 : 0); i++) {
				float degInRad = i*DEG2RAD;
				glVertex2f(x + cos(degInRad)*radius,y + sin(degInRad)*radius);
			}
		glEnd();
	}
	void circleGrad(float x, float y, float radius,
		const color::color& inner, float inneralpha, const color::color& outer, float outeralpha,
		float quality)
	{
		const float DEG2RAD = 3.14159/(22.5*quality);
		glBegin(GL_TRIANGLE_FAN);
			glColor4f(inner.r, inner.g, inner.b, inneralpha);
			glVertex2f(x,y);
			glColor4f(outer.r, outer.g, outer.b, outeralpha);
			for (int i=0; i<(45*quality)+1; i++) {
				float degInRad = i*DEG2RAD;
				glVertex2f(x + cos(degInRad)*radius,y + sin(degInRad)*radius);
			}
		glEnd();
	}
}