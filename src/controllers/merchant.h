#pragma once
#include "../util.h"
#include "../ui.h"
#include "../ui/label.h"
#include "../ui/statbar.h"
#include "../ui/button.h"
#include "../ui/field.h"
#include <list>
#include "../data/map.h"
#include "../data/ship.h"
namespace Controllers {
	class Merchant : public ui::Controller {
		void updateCargo();
		void updateCommodities();
		void redrawScrollpane();
		void clearComGroups();
		bool needsToRedraw, needsToClearHoverwidget;
		class QuantityDialog : public ui::Group {
			void close();
			ui::Button confirm, cancel, max, tons, freespace;
			ui::Label comName, price, weight, comDesc;
			ui::Field f_quantity;
			color::scheme* color;
			bool closing;
			public:
				QuantityDialog(Merchant*);
				Merchant* datasrc;
				data::commodity* commodity;
				void update();
				data::quantity_t getMax(), fromTons(data::weight_t);
				void render() override;
				void callback(void*) override;
				void loop() override;
				bool selling();
				void handle(const ui::event& e) override;
		};
		public:
			ui::ReverseGroup g_scrollpane; bool g_scrollpane_dragging;
			data::ship* pship;
			data::location* buyFrom, *sellTo;
			struct comm_grp {
				comm_grp(
					Merchant*, // parent
					data::inventory<data::commodity*>::quantity&,
					float y,
					std::string name,
					ui::Group& scrollpane,
					bool sellable
				);
				data::commodity* commodity;
				ui::Button button;
				ui::Label label,price,origprice;
			};
			QuantityDialog quantitydialog;
			bool quantitydialog_visible;
			ui::StatBar cargo;	ui::StatBar::Series cargodata[3];
			ui::Button b_cancel, b_finalize;
			ui::Button::Group b_modeselector;
				ui::Button b_modeselector_buy, b_modeselector_sell;
			ui::Label money;
			std::list<comm_grp*> commodities;
			data::inventory<data::commodity*> curtrade;
			data::system* sys;
			data::weight_t tradeWeight();
			data::quantity_t getMax(data::commodity*);
			void modTrade(data::commodity* c, data::quantity_t q);
			void finishTrade();
			Merchant(data::system*);
			~Merchant();
			void handle(const ui::event& e) override;
			void render() override; // hack to enable GL drawing between widgets.
			void callback(void*) override;
			void auxcallback(void*) override;
			void loop() override;
	};
}