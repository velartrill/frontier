#pragma once
// nasty hack to resolve circular dependency issue
namespace data {
	class location;
	class system;
	class ship;
	struct location_t {	// provide runtime type data
		enum class Type {planet, ship, station, system} type;
		location* loc;
	};
	class locatable { public: 
		location_t loc;
		system* getSystem() const;
		ship* getShip() const;
	};
}