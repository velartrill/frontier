#pragma once
#include "../ui.h"
#include "../color.h"
#include "../text.h"
namespace ui {
	class StatBar : public ui::Widget { public:
		struct Series {
			color::scheme* color;
			float val;
			const char* name;
		};
		float w, h, max, lastClick;
		unsigned char seriesc;
		Series* series;
		Series* hoverseries;
		color::scheme* color;
		Text tooltipText;
		StatBar(float w, float h, float max=1);
		StatBar(float x, float y, float w, float h, float max=1);
		void render() override;
		void loop() override;
		void handle(const ui::event&) override;
		bool inhitbox(float x, float y) override;
	};
}