#pragma once
#include "../ui.h"
#include "../ui/label.h"
#include "../ui/imgbtn.h"
#include "../data/map.h"
#include "../image.h"
namespace Controllers {
	class SystemMap : public ui::Controller {
		ui::Label l_system, l_faction;//, l_trade;
		Image i_ship, i_trade, i_sysinfo;
		ui::ImgBtn b_ship, b_trade, b_sysinfo;
		data::system* sys;
		float drag;
		bool dragging;
		struct posData {
			float cx, cy, distfactor, sizefactor;
		};
		posData getPosData();
		// void dragUpdate();
		public:
			SystemMap(data::system*);
			void render() override;
			// void loop() override;
			void handle(const ui::event&) override;
			void callback(void*);
	};
}