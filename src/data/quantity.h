#pragma once
#include "types.h"
#include <list>
#include "../util.h"
#include <cassert>
namespace data {
	template <typename T> class inventory { public:
		struct quantity {
			T			ct;
			quantity_t	q;
			price_t		origprice;
		};
		std::list<quantity> items;
		bool add(T cti, quantity_t number = 1, price_t origprice = 0) {
			for (quantity& q : items) {
				if(q.ct==cti) {
					q.origprice=(q.origprice*q.q + origprice*number)/(q.q+number); //Calculate a rough average
					q.q+=number;
					return false;
				}
			}
			/* otherwise */ items.push_back({cti, number, origprice}); return true;
		}
		bool remove(T cti, quantity_t number = 1) {		//return true if redraw required
			for (auto i = items.begin(); i!=items.end(); i++) {
				if(i->ct==cti) {
					if (number == i -> q) { items.erase(i); return true; }
					i->q-=number;
					return false;
				}
			}
			/* otherwise */ items.push_back({cti, -number, 0}); // assume this is a transactional inventory
			return true;
		}
		quantity_t count(T cti) {
			for (quantity& q : items) if(q.ct==cti) return q.q;
			/* otherwise */ return 0;
		}
		
		void transact(const inventory<T>& nt) {
			// perform a differential transaction with nt
			for (auto& i : nt.items) {
				if ((i.q) > 0) add(i.ct,i.q,i.origprice);
				else if ((i.q) < 0) remove(i.ct,i.q);
			}
		}
		void reverseTransact(const inventory<T>& nt) {
			for (auto& i : nt.items) {
				if ((i.q) > 0) remove(i.ct,i.q);
				else if ((i.q) < 0) add(i.ct,i.q,i.origprice);
			}
		}
		data::price_t totalPrice(data::system* sys) {
			if (items.empty()) return 0;
			data::price_t total = 0;
			for (auto& q : items) {
				total += q.ct->priceIn(sys) * q.q;
			}
			return total;
		}
		data::price_t totalOrigPrice() {
			if (items.empty()) return 0;
			data::price_t total = 0;
			for (auto& q : items) {
				total += q.origprice * q.q;
			}
			return total;
		}
		quantity& operator[] (T cti) {
			for (quantity& q : items) if(q.ct==cti) return q;
			throw "Not found.";
		}
	};
}
