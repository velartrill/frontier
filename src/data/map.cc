#include "map.h"
namespace data {
	bool system::habitable() {
		for (int pl = 0; pl < planetc; pl++) {
			if (planets[pl].type >= planet::Type::oceanic && planets[pl].type <= planet::Type::grassy) {
				return true;
			}
		}
		return false;
	}
	void system::resetMarket() {
		if (!habitable()) return;
		for(commodity* c : products) {
			quantity_t q = c->quantityIn(this); // how much of this commodity can occur here?
			if (q>0) commodities.add(c,q-commodities.count(c));
		}
	}
	void system::marketTick() {
		if (!habitable()) return;
		for(commodity* c : products) {
			if (rand() % 3==0) { // markets update roughly once every three days
				quantity_t current = commodities.count(c);
				quantity_t q;
				if (!c->minable_det) { // is this a consumer good?
					//$("Max % in %: %",c->name,name,c->quantityIn(this));
					q = std::max((quantity_t)(c->quantityIn(this) / c->rarity), (quantity_t)1);
						// how much are we refilling? Min 1, which is a bit of a hack
				} else { // or a mineral?
					q = 0;
					for (size_t p = 0; p<planetc; p++)
						q += c->minable(this,&planets[p]);
					//$("Max % in %: %",c->name,name,q);
				}
				q+=current;
				if (q==0) continue;
				q = (rand()%q) - current;
				if 		(q>0)	commodities.add(c,q);
				else if	(q<0)	commodities.remove(c,-q);
			}
		}
		//TODO consume commodities
	}
	bool system::afflictedBy(condition::prototype* p) const {
		for (const condition& c : conditions)	if (c.proto == p) return true;
		/* otherwise */ return false;
	}
	const system::Type system::types[] = {
		{"Yellow Dwarf",	{.7,.7,.4}},
		{"Red Giant",		{.7,.4,.4}},
		{"Blue Giant",		{.4,.6,.7}}
	};
}