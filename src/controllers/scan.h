#pragma once
#include "../ui.h"
#include "../form.h"
#include "../data/map.h"
#include "../ui/button.h"
#include "../ui/statbar.h"
#include "../ui/table.h"
namespace Controllers {
	class Scan : public ui::Form {
		class StarWindow : public ui::Widget { public:
			StarWindow(float x, float y, float size, data::system* sys);
			color::scheme* color;
			void render() override;
			void loop() override;
			void handle(const ui::event&) override;
			float size;
			data::system* sys;
		};
		data::system* sys;
		ui::Button b_jump, b_cancel;
		ui::StatBar s_jumpstat;
		ui::StatBar::Series jumpdata[2];
		ui::Label l_energy;
		StarWindow w_star;
		ui::Table t_details;
		public:
			Scan(data::system*);
			void callback(void*) override;
			void handle(const ui::event& e) override;
	};
}