#include "merchant.h"
#include "../window.h"
#include <glfw.h>
#include "../color.h"
#include "../tostring.h"
#include "../data/universe.h"
#include "../alert.h"
#include "../strings.h"
namespace Controllers {
	Merchant::QuantityDialog::QuantityDialog(Merchant* _datasrc) :
		/*			Label				X				Y				W		H	Color */
		confirm		("Confirm",			225,			0,				225,	80,	&color::green),
		cancel		("Cancel",			0,				0,				225,	80,	&color::red),
                                                                		
		max			("Maximum",			0,				160,			450/3,	45),
		tons		("Tons to Units",	max.w,			max.y,			max.w,	45),
		freespace	("Leave Free",		tons.x+tons.w,	max.y,			max.w,	45),
		comName		("",				10,				300,			450-20,	40.0f),
		price		("",				0,				max.y-max.h-15,	450/2,	45.0f),
		weight		("",				450/2,			max.y-max.h-15,	450/2,	45.0f),
		comDesc		("",				0,				-40,			452,	16.0f, {1,1,1}),
		f_quantity	(					140,			215,			180,	35,"",true)
	{
		parent=_datasrc;
		datasrc=_datasrc;
		w=450;	x=(window::width - w) / 2;
		h=350;	y=(window::height - h) / 2;
		price.text.align(Font::align::center);
		weight.text.align(Font::align::center);
		price.shadow=true;
		weight.shadow=true;
		add(&comName							);
		add        (    &f_quantity    );
		add(&max);  add(&tons);   add(&freespace);
		add(&price		);	add(&weight			);
		add(&cancel		);	add(&confirm		);

		comDesc.text.align(Font::align::center);
		add(			&comDesc				);
	}
	void Merchant::QuantityDialog::update() {
		comName.text.str=std::string(commodity->name);
		f_quantity.setText("");
		f_quantity.takeFocus();
		callback(&f_quantity);
		if (selling()) {
			freespace.visible=false;
			color=&color::brown;
		} else {
			freespace.visible=true;
			color=&color::purple;
		}
		max.btncolor=color;
		tons.btncolor=color;
		freespace.btncolor=color;
		closing=false;
		if (commodity->desc) comDesc.text.str=std::string(commodity->desc);
		else comDesc.text.str="";
	}
	void Merchant::QuantityDialog::render() {
		float x = abx(), y = aby();
		// draw blackout
		glEnable(GL_BLEND);
		glColor4f(0,0,0,.7);
		glBegin(GL_QUADS);
			glColor4f(0,0,0,.7);
				glVertex2f(0,0);
				glVertex2f(window::width,0);
				glVertex2f(window::width,window::height);
				glVertex2f(0,window::height);
			color::apply(*color, ui::Widget::off, color::colortype::bg,.4);
				glVertex2f(x,y);
				glVertex2f(x+w,y);
			color::apply(*color, ui::Widget::off, color::colortype::bg,.7);
				glVertex2f(x+w,y+h);
				glVertex2f(x,y+h);
		glEnd();
		if (commodity->desc) {
			//draw description caption box
			glColor4f(0,.1,.4,.8);
			float bx = comDesc.abx(), by=comDesc.aby() + 17;
			glBegin(GL_QUADS);
				glVertex2f(bx-5,by+5);
				glVertex2f(bx+comDesc.w+5,by+5);
				glVertex2f(bx+comDesc.w+5,by+comDesc.text.height()+12);
				glVertex2f(bx-5,by+comDesc.text.height()+12);
			glEnd();
		}
		glDisable(GL_BLEND);
		Group::render();
	}
	bool Merchant::QuantityDialog::selling() {
		return datasrc->buyFrom == datasrc->pship;
	}
	void Merchant::QuantityDialog::loop() {
		if (closing) {
			if (hoverwidget!=nullptr) {
				hoverwidget->state=off;
				hoverwidget=nullptr;
			}
			datasrc -> quantitydialog_visible=false;
		}
		Group::loop();
	}
	void Merchant::QuantityDialog::close() {
		closing=true; // had to switch to queue-style so that the buttons could be appropriately deactivated.
	} 
	
	void Merchant::QuantityDialog::handle(const ui::event& e) {
		// we don't actually need to check except for [ENTER] - keyboard shortcuts are easier that way
		//if (focuswidget==nullptr) {
			if (e.type==ui::eventtype::key && e.key.state==ui::pressed) {
			
				switch (e.key.key) {
					case GLFW_KEY_ESC: close(); return;
					case GLFW_KEY_ENTER:
					case GLFW_KEY_KP_ENTER: if (focuswidget==nullptr) { callback(&confirm); return; } break;
					case 'T': callback(&tons); return;
					case 'M': callback(&max); return;
					case 'F': callback(&freespace); return;
				}
			}
		//}
		Group::handle(e);
	}
	data::quantity_t Merchant::QuantityDialog::fromTons(data::weight_t maxweight) {
		data::weight_t curShipWeight = datasrc->pship->weight() + datasrc->tradeWeight();
		if (maxweight > datasrc->pship->shipclass->maxweight - curShipWeight)
			throw "Your ship doesn't have that much free space!";
		else {
			return std::min(datasrc->getMax(commodity),(data::quantity_t)(maxweight/commodity->weight));
		}
	}
	void Merchant::QuantityDialog::callback(void* w) {
		if (w==&cancel) {
			close();
		} else if (w==&confirm) {
			data::quantity_t q = stringTo<data::quantity_t>(f_quantity.text);
			if (q > datasrc->getMax(commodity)) {
				window::alert(!selling() ? "You can't buy that many." : "You can't sell that many.",&color::red);
			} else {
				datasrc->modTrade(commodity,q);
				close();
			}
		} else if (w==&max) {
			f_quantity.setText(toString(datasrc->getMax(commodity)));
			callback(&f_quantity);
		} else if (w==&tons) {
			if (f_quantity.text.empty()) {f_quantity.takeFocus(); window::alert("Number of tons required.",&color::red); return;}
			try {
				f_quantity.setText(toString(fromTons(stringTo<data::weight_t>(f_quantity.text))));
			} catch (const char* n) {
				window::alert(n,&color::red);
			}
			callback(&f_quantity);
		} else if (w==&freespace) {
			if (f_quantity.text.empty()) {
				f_quantity.takeFocus();
				window::alert("Amount of space to be left free required.",&color::red);
				return;
			}
			data::weight_t curShipWeight = datasrc->pship->weight() + datasrc->tradeWeight();
			f_quantity.setText(toString(
				fromTons((datasrc->pship->shipclass->maxweight - curShipWeight) - stringTo<data::weight_t>(f_quantity.text))
			));
			// is this what it's like to write Haskell?
			callback(&f_quantity);
		} else if (w==&f_quantity) {
			data::quantity_t q = f_quantity.text.empty() ? 0 : stringTo<data::quantity_t>(f_quantity.text);
			if (q > 9999999) q = 9999999, f_quantity.setText(toString(q)); // FIXME find a better way of dealing with unacceptably large numbers
			/* end stupidity filters */

			data::price_t f = q * commodity->priceIn(datasrc->sys);
			price.text.str=strings::price(f);
			weight.text.str=strings::weight(q * commodity->weight);

			if (f > data::player->money && !selling()) {
				price.color=color::red.base.bg;
			} else {
				price.color=color::green.base.fg;
			};

			data::weight_t curShipWeight = datasrc->pship->weight() + datasrc->tradeWeight();
			if (curShipWeight+q*commodity->weight > datasrc->pship->shipclass->maxweight) {
				weight.color=color::red.base.bg;
			} else {
				weight.color=color::green.base.fg;
			}
		}
	}
}