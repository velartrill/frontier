#include "map.h"
#include <cstdlib>
#include "../util.h"
namespace data {
	namespace {
		inline bool facType(const system* s, Faction::Ideology f, bool defl) {
			return s->faction ? s->faction->ideology==f : defl; }
	
		quantity_t f_stddist	(const system*, unsigned int param) { return rand()%param; }
		quantity_t f_consumer	(const system* s, unsigned int param) {
			if (s->faction==nullptr) return rand()%param;
			else if (s->faction->ideology==Faction::Ideology::consumerist) return rand()%param*2;
			else return 0;
		}
		quantity_t f_cuisine		(const system* s, unsigned int param)	{
			bool socialist = facType(s,Faction::Ideology::socialist,false);
			return socialist ? 0 : rand()%10 * s->techlevel;
		}
		quantity_t f_hautecuisine	(const system* s, unsigned int param)	{
			bool socialist = facType(s,Faction::Ideology::socialist,false);
			return socialist ? 0 : rand()%4 * s->techlevel;
		}
		quantity_t f_drugs			(const system* s, unsigned int param)	{
			return s->techlevel > 1 ? rand() % (unsigned int)((float)(9.0f*s->techlevel))*((float)param/10.0f) : 0; //sorry about this!
		}
		quantity_t f_industrial		(const system* s, unsigned int param)	{ return s->techlevel > 2 ? rand()%(5*s->techlevel) : 0; }
		quantity_t f_hitech			(const system* s, unsigned int param)	{ return s->techlevel > 5 ? rand()%(param*(s->techlevel-5)) : 0; }
		quantity_t f_ultratech		(const system* s, unsigned int param)	{ return s->techlevel >= 17 ? rand()%(param*(s->techlevel-16)) : 0; }
		quantity_t f_lotech			(const system* s, unsigned int param)	{
			return s->techlevel < 5 ?		s->techlevel == 0	? param*1.5
																: rand()%(param/s->techlevel)
											: 0;
		}
		quantity_t f_ultrarare		(const system* s, unsigned int param)	{ return (rand()%30 == 1 ? 1 : 0);}
	
		bool marketcontrol			(const system* s, float level)			{
			return s->marketcontrol <= level || s->afflictedBy(&conditions::anarchy);
		}
		bool f_sellable_weapons		(const system* s, unsigned int param)	{ return marketcontrol(s,.6); }
		bool f_sellable_nosocialist	(const system* s, unsigned int param)	{ return !facType(s,Faction::Ideology::socialist,false); }
		bool f_sellable_communist	(const system* s, unsigned int param)	{ return facType(s,Faction::Ideology::communist,false); }
		bool f_sellable_nlweapons	(const system* s, unsigned int param)	{ return marketcontrol(s,.5); }
		bool f_sellable_recdrugs	(const system* s, unsigned int param)	{ return marketcontrol(s,.4) || f_sellable_communist(s,0); }
		bool f_sellable_harddrugs	(const system* s, unsigned int param)	{ return marketcontrol(s,.2); }
		bool f_sellable_anarchy		(const system* s, unsigned int param)	{ return marketcontrol(s,.1); }
		bool f_sellable_nuke		(const system* s, unsigned int param)	{ return marketcontrol(s,0); }
	
		quantity_t f_minable_stddist(const system* s, const planet* p, rarity_t r) { return rand()%50 / r; }
		
		float p_lethal (const system*s, unsigned int param, bool* use) {
			if (s->afflictedBy(&conditions::war)) { // TODO make authoritarian systems prefer lethal weapons for unrest
				*use=true;
				return .25*param;
			} else {
				*use=false;
				return 0;
			}
		}
		float p_nonlethal (const system*s, unsigned int param, bool* use) {
			if (s->afflictedBy(&conditions::unrest)) {
				*use=true;
				return .17*param;
			} else {
				*use=false;
				return 0;
			}
		}
		float p_food (const system* s, unsigned int param, bool* use) {
			if (s->afflictedBy(&conditions::famine)) {
				*use=true;
				return .25*param;
			} else if (s->afflictedBy(&conditions::shortages)) {
				*use=true;
				return .10*param;
			} else {
				*use=false;
				return 0;
			}
		}
		float p_drink (const system* s, unsigned int param, bool* use) {
			if (s->afflictedBy(&conditions::drought)) {
				*use=true;
				return ((float)param)/10.0f;
			} else {
				*use=false;
				return 0;
			}
		}
	}
	commodity commodities[] = {
		/* Materials */
		{"Iron",		f_stddist,20,	3,		20,		nullptr,0,nullptr,0, f_minable_stddist, nullptr},
		{"Hydrogen",	f_stddist,40,	2,		1,		nullptr,0,nullptr,0, f_minable_stddist,
			"The oldest and most abundant element in the universe. A key component of dihydrogen monoxide, a multi-purpose "
			"chemical agent human civilization heavily relies on."},
		{"Titanium",	f_stddist,10,	5,		10,		nullptr,0,nullptr,0, f_minable_stddist, nullptr},
		{"Uranium",		f_stddist,3,	60,		52,		nullptr,0,nullptr,0, f_minable_stddist, nullptr},
		{"Gold",		f_stddist,5,	35,		48,		nullptr,0,nullptr,0, f_minable_stddist, nullptr},
		{"Neutronium",
			[](const system* s, unsigned int param) -> quantity_t{
				if (s->techlevel >= 15) return rand()%15; else return 0;
			},0,						100,	2000,	nullptr,0,nullptr,0, f_minable_stddist, nullptr},
			// making allowances for gameplay here; neutronium is actually around 40 quadrillion kg/m^3
			// (or so says WolframAlpha, anyway)
		{"Unobtanium", f_ultratech,4,	75,		25,		nullptr,0,nullptr,0, nullptr,
			"An ultratech frictionless material with the highest tensile strength of any substance in "
			"the known universe."},
		{"Phlebotinum", f_ultratech,2,	82,		35,		nullptr,0,nullptr,0, nullptr,
			"An exotic substance with wide-ranging applications, particularly in hyperdrive construction, "
			"faster-than-light communication, high-energy weapons, and gravity generators."},
		{"Space dust",		f_stddist,7,	37,		3,		nullptr,0,nullptr,0,
			[](const system* s, const planet* p, rarity_t) -> quantity_t{
				return p->habitable() ? rand() % 10 : 0;
			},
			"A minor precognitive mostly used for cheating at card games."
		},
		/* Goods */
		{"Clean Water",		[](const system* s, unsigned int param) -> quantity_t{
								if (facType(s,Faction::Ideology::socialist,false) ||
									facType(s,Faction::Ideology::communist,false)) return f_stddist(s,param);
								else
									return f_stddist(s,param/10) * s->techlevel;
							},			   20,		3,		1, p_drink,30,	nullptr,0, nullptr,
			"Purified water, treated to the standards of a hi-tech civilization, and guaranteed not to "
			"irradiate or infect its consumers."},
		{"Water",				f_consumer,20,		2,		1, p_drink,16,	nullptr,0, nullptr,
			"A widely used industrial coolant. You can probably drink it without getting parasites, "
			"too.\nProbably."},
		{"Dirty Water",			f_consumer,40,		1,		1, p_drink,12,	nullptr,0, nullptr,
			"Water affected by radiation or biological contamination. Drinking this probably will do "
			"nasty things to your insides, but it's better than death by dehydration. Sometimes."},
		{"Towels",				f_consumer,3,		2,		6, nullptr,0,	nullptr,0, nullptr, nullptr},
		{"Paper",				f_lotech,30,		1,		6, 
		
			[](const system* s, unsigned int param, bool* use) -> float {
				if (s->afflictedBy(&conditions::bureaucracy))	{ *use=true; return 2.3; }
														 else	{ *use=false; return 0; }
			},0,															nullptr,0, nullptr,
			"Used little in high-tech civilizations, paper is an old-fashioned medium for communication and record-"
			"keeping made from wood products. Paper has several disadvantages compared with electronic databanks, "
			"as it is not easily reusable, requires physical ink to produce visible markings upon it, and cannot "
			"be duplicated without specialized technology. Nonetheless, even in high-tech civilizations, paper retains "
			"security applications as it significantly harder to steal than digital data."},
		{"Watches",				f_consumer,10,		3,		5, nullptr,0,	nullptr,0, nullptr, nullptr},
		{"Games",				f_consumer,10,		9,		7, nullptr,0,	nullptr,0, nullptr, nullptr},
		{"Rations",				f_consumer,15,		11,		5, p_food,6,	nullptr,0, nullptr, 
			"They'll keep you alive and nothing else. Most rations in the galaxy are Sierra Corporation RationPaks™ dating "
			"back to the late Second Empire, which were mass-produced and sold in the trillions galaxywide during the Great "
			"War, the single most profitable conflict in galactic history. The Society, however, produces its own emergency "
			"rations of considerably higher quality."},
		{"Fast Food",			f_consumer,25,		8,		6, p_food,3,	nullptr,0, nullptr, nullptr},
		{"Fine Food",			f_cuisine,0,		30,		7, p_food,10,	nullptr,0, nullptr, nullptr},
		{"Haute Cuisine",		f_hautecuisine,0,	50,		7, p_food,15,	nullptr,0, nullptr, 
			"In foodie parlance, the term \"haute cuisine\" is strictly restricted to describing the output of "
			"Altair-trained hyperchefs, and only the galaxy's wealthiest elites can afford to eat it on a regular "
			"basis."},
		/* B2B */
		{"Machinery",			f_industrial,0,		20,		225, nullptr,0,nullptr,0, nullptr, nullptr},
		{"Robots",				f_hitech,3,			54,		650, nullptr,0,nullptr,0, nullptr, nullptr},
		/* Weapons */
		{"Combat Knives",		f_stddist,20,		10,		40, p_lethal,1,		f_sellable_weapons,0,	nullptr, nullptr},
		{"Firearms",			f_stddist,10,		12,		40, p_lethal,10,	f_sellable_weapons,0,	nullptr, nullptr},
		{"Stunners",			f_stddist,15,		17,		40, p_nonlethal,5,	f_sellable_nlweapons,0, nullptr, nullptr},
		{"Rocket Launchers",	f_stddist,3,		35,		95,	p_lethal,25,	f_sellable_weapons,0,	nullptr, nullptr},
		{"Nuclear Warhead",		f_ultrarare,0,		255,	945,p_lethal,100,	f_sellable_nuke,0,		nullptr, nullptr},
		/* Drinks */
		/* Drugs */
		{"Marijuana",			f_drugs,5,			30,		2,	nullptr,0,		f_sellable_recdrugs,0, nullptr, 
			"A common mood-improving drug, marijuana is used in all but the most conservative systems."},
		{"Glit",				f_drugs,3,			37,		2,	nullptr,0,		f_sellable_recdrugs,0, nullptr, nullptr},
		{"Yock",				f_drugs,2,			40,		2,	nullptr,0,		f_sellable_recdrugs,0, nullptr, 
			"The street name for the ground leaves of the Altairian hypershrub, yock is a powerful hallucinogen."},
		{"Snidge",				f_drugs,1,			50,		2,	nullptr,0,		f_sellable_recdrugs,0, nullptr, 
			"Snidge produces intense euphoria coupled with total sleep paralysis. "
			"While harmless in the long term, potential for malicious use and addiction, coupled with the effect "
			"of rendering its users helpless in an unsafe situation, prompts most governments to strictly limit "
			"its usage to publicly-run or heavily-regulated drughouses."},
		/* Hard drugs */
		{"Thrunk",				f_drugs,4,			60,		2,	nullptr,0,		f_sellable_harddrugs,0, nullptr,
			"Made from the pulverized bones of the Merlydian blood-rat and the sap of the Altairian hypercactus, "
			"thrunk is immensely popular among the Empire's wealthier gangs, as it deadens pain response more "
			"effectively than any painkiller outside the Aegis, and counters symptoms of PTSD and depression. "
			"Long-term use is extremely addictive, and withdrawal after a year is usually slowly and painfully fatal."},
		{"Craff",				f_drugs,3,			65,		2,	nullptr,0,		f_sellable_harddrugs,0, nullptr,
			"A favorite of the Empire's Mafia, craff is a strong psychedelic and is widely "
			"considered to be an effective aphrodisiac. Addiction is rapid, and withdrawal can lead "
			"to months of depression, anxiety, hallucination, and in severe cases, fatal death."},
		{"Hyperyock",		f_drugs,2,			73,		2,	nullptr,0,		f_sellable_harddrugs,0, nullptr,
			"Yock cut with Altairian hyperlobster juice produces hyperyock, which can and will cause permanent "
			"memory loss, psychotic episodes, and a complete shutdown of executive function in the brain."},
		{"Skurp",				f_drugs,1,			80,		2,	nullptr,0,		f_sellable_harddrugs,0, nullptr,
			"Produced from industrial runoff and the psychedelic venom of the Altarian hyperscorpion, and all but "
			"guaranteed to cause permanent neurological damage, necrosis of the liver, and withdrawal symptoms "
			"including weeks of uncontrollable projectile vomiting and complete and total hair loss. Skurp is "
			"banned in all civilized systems."},
	};
	const size_t commodityc = sizeof(commodities)/sizeof(commodity);
	price_t commodity::priceIn(const system* s) const {
		//const quantity_t nOf = s->commodities.quantityIn(this);
		price_t price = std::max((unsigned int) (rarity*(35-s->techlevel))/* /std::min(nOf*.2,1.0) */,1u);
		if (price_det!=nullptr) {
			bool use;
			float fac = (*price_det)(s,price_det_param,&use);
			if (use) return price*fac;
		}
		return price;
	}
	quantity_t commodity::quantityIn(const system* s) const {
		return (*quantity_det)(s,quantity_det_param);
	}
}
