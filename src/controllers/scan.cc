#include "scan.h"
#include "../window.h"
#include "../data/ship.h"
#include "../data/universe.h"
#include "../circle.h"
#include "../strings.h"
namespace Controllers {
	Scan::Scan(data::system* _sys) :
		b_jump		("Jump",			window::width-220,	20,200,80,&color::red),
		b_cancel	("Cancel",			20,					20,200,80,&color::purple),
		s_jumpstat	(20,				160,				window::width-40,	60),
		l_energy	("Energy Usage",	0,					130,window::width,	19),
		w_star		(20,				240,				window::height-350, _sys),
		t_details	(40+w_star.size, 	220+w_star.size, window::width-w_star.size-70, window::height-500, 200)
	{
		sys=_sys;
		color=(sys->faction!=nullptr ? sys->faction->color : &color::blue);
		b_cancel.btncolor=color;
		addtitle(sys->name);
		add(&b_cancel);
		add(&w_star);
		w_star.color=color;
		data::ship& pship = *data::player->getShip();
		if (pship.distanceTo(sys) <= pship.range()) {
			l_energy.text.align(Font::align::center);
			add(&l_energy);
			s_jumpstat.color=color;
			s_jumpstat.max=pship.shipclass->energy;
			if (pship.canJumpTo(sys)) {
				add(&b_jump);
				jumpdata[0].name="Current Energy";
				jumpdata[0].val=pship.energy;
				jumpdata[0].color=&color::red;
				
				jumpdata[1].name="After Jump";
				jumpdata[1].val=pship.energy-pship.jumpEnergyCost(sys);
				jumpdata[1].color=&color::green;
	
				s_jumpstat.series=jumpdata;
				s_jumpstat.seriesc=2;
				l_energy.color=color->base.fg;
			} else {
				jumpdata[0].name="Energy Needed";
				jumpdata[0].val=pship.jumpEnergyCost(sys);
				jumpdata[0].color=&color::purple;
				
				jumpdata[1].name="Energy Available";
				jumpdata[1].val=pship.energy;
				jumpdata[1].color=&color::green;
	
				s_jumpstat.series=jumpdata;
				s_jumpstat.seriesc=2;
				s_jumpstat.max=pship.shipclass->energy;
				l_energy.text.str="Energy Requirements";
			}
			t_details.addrow("Star Type",std::string(data::system::types[(size_t)sys->type()].name));
			if (sys->faction!=nullptr)
				t_details.addrow("Faction",sys->faction->name);
			t_details.addrow("Market Control",strings::percentage(sys->marketcontrol));
			t_details.addrow("Tech Level",strings::techlevel(sys->techlevel));
			t_details.addrow("Travel Time",strings::timespan(data::player->getShip()->jumpTimeTo(sys)));
			t_details.color=color;
			add(&t_details);
			add(&s_jumpstat);
		}
		
	}
	void Scan::callback(void* w) {
		if (w==&b_cancel) {
			window::back();
		} else if (w==&b_jump) {
			data::ship& ship = *data::player->getShip();
			data::tick(ship.jumpTimeTo(sys));
			ship.jumpTo(sys);
			window::back();
		}
	}
	void Scan::handle(const ui::event& e) {
		if (e.type==ui::eventtype::key && e.key.key==GLFW_KEY_ESC) {
			window::back();
		} else Form::handle(e);
	}
	Scan::StarWindow::StarWindow(float _x, float _y, float _size, data::system* _sys) { x=_x, y=_y, size=_size, sys=_sys; };
	void Scan::StarWindow::render() {
		glEnable(GL_BLEND);
			const float xpos=abx(), ypos=aby();
			glColor4f(0,0,0,.6);
			glBegin(GL_QUADS);
				glVertex2f(xpos,ypos);
				glVertex2f(xpos,ypos+size);
				glVertex2f(xpos+size,ypos+size);
				glVertex2f(xpos+size,ypos);
			glEnd();
			color::apply(*color, ui::Widget::off, color::colortype::border, 0.8f);
			glBegin(GL_LINE_STRIP);
				glVertex2f(xpos,ypos);
				glVertex2f(xpos,ypos+size);
				glVertex2f(xpos+size,ypos+size);
				glVertex2f(xpos+size,ypos);
				glVertex2f(xpos,ypos);
			glEnd();
		
		//woo copypasta
			/*draw glow*/
			const color::color& cl = data::system::types[(size_t)sys->type()].color; //evil? try "efficient"
			draw::circleGrad(xpos+(size/2), ypos+(size/2), sys->size*(size/2-20), cl,1, cl, 0, .4);

			/* draw the center */
			glEnable(GL_MULTISAMPLE);
			glColor3f(1,1,1);
			draw::circle(xpos+(size/2), ypos+(size/2), sys->size*((size/2-20)/3), .7, true);
			glDisable(GL_MULTISAMPLE);
		//end copypasta
		glDisable(GL_BLEND);
	}
	void Scan::StarWindow::handle(const ui::event&) {};
	void Scan::StarWindow::loop() {};
}