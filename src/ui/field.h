#pragma once
#include "../ui.h"
#include <string>
#include "../text.h"
namespace ui {
	class Field : public Widget {
		Text _text_obj;
		double cursor;
	public:
		bool numeric;
		Field(float w, float size=20.0f, std::string _text = "", bool numeric = false);
		Field(float x, float y, float w, float size=20.0f, std::string _text = "", bool numeric = false);
		float w, size;
		std::string text;
		void setText(std::string);
		void render();
		void loop();
		void handle(const event&);
		bool inhitbox(float x, float y);
		void onBlur() override;
		void takeFocus();
	};
}