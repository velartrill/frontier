#pragma once
#include "../ui.h"
#include "../ui/button.h"
#include "../ui/label.h"
#include "../ui/table.h"
#include "../data/universe.h"
#include <vector>
namespace Controllers {
	class BuyHyperdrive : public ui::Controller {
		ui::Label l_title;
		ui::Group driveList;
		ui::Group driveView;
			ui::Button b_buy;
			ui::Label l_header, l_desc;
			ui::Table t_stats;
				ui::Table::row*	r_price, *r_range, *r_efficiency, *r_speed, *r_techlevel;
		bool driveViewVisible; //haaack
		struct DriveSignalObject { ui::Button* b; data::item* i; } *selection;
		
		
	public:
		BuyHyperdrive();
		~BuyHyperdrive();
		color::scheme* factionColor;
		void callback(void*w) override;
		void handle(const ui::event&) override;
		void render() override;
	};
}