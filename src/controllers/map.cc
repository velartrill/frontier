#include "map.h"
#include "../window.h"
#include "../alert.h"
#include "../data/universe.h"
#include "../strings.h"
#include "../tostring.h"
#include <glfw.h>
#include "../circle.h"
#include "../text.h"
#include "../font.h"
#include "../util.h"
#include <cmath>
#include "../ui.h"
#include "scan.h"
#include "systemmap.h"
namespace Controllers {
	Map::Map() :
		sysname("",Font::face::zekton,36.0f),
		facname("",Font::face::zekton,18.0f),
		date("",window::width,Font::align::center,Font::face::zekton,14.0f),
		
		shipname("",Font::face::zekton,36.0f),
		shipclass("",Font::face::zekton,18.0f),
		
		b_show_factions("Factions",0,0,180,50, &color::brown),
		b_show_informants("Informants",180,0,180,50, &color::brown),
		b_show_none("None",360,0,180,50, &color::brown),
		
		b_galactic_view("Galactic View",window::width-180,0,180,50, &color::blue),
		b_find_system("Find System",window::width-360,0,180,50, &color::red)
	{
		radius=window::smallest_dimension() - 100;
		distfactor = (radius / data::usize) / 2;
		sizefactor = 50 / data::usize;
		hlt=nullptr;
		mode=local;
		update();
		b_show=&b_show_factions;
		b_show_factions.group=&b_show;
		b_show_informants.group=&b_show;
		b_show_none.group=&b_show;
		add(&b_show_factions);
		add(&b_show_informants);
		add(&b_show_none);
		b_galactic_view.toggle=ui::Button::togglestate::toggle;
		add(&b_galactic_view);
		add(&b_find_system);
		
		dialog=nullptr;
		hilitesys=nullptr;
	};
	void Map::render() {
		glColor3f(1,1,1);
		sysname.render(20,window::height-46);
		if (cursys->faction!=nullptr) {
			color::apply(*cursys->faction->color, off, color::colortype::fg, .7);
			facname.render(20,window::height-(46+12+18));
		}
		if (data::player->loc.type==data::location_t::Type::ship) {
			glColor4f(1,1,1,.8);
			shipname.posrender();
			glColor4f(1,1,1,.5);
			shipclass.posrender();
		}
		const float cx = window::width / 2, cy = window::height / 2 + 35;
		data::system& psys = *cursys;
		data::ship& pship = *data::player->getShip();
		float df2;
		if (mode==local) {
			df2=(pship.range()*distfactor) / (radius/2); //scale back up. this is heinous. FIXME FIXME FIXME
		} else df2=1;
		for (int i=0;i<data::systemc;i++) { const data::system& s = data::systems[i];
			const bool starInRange = pship.distanceTo(&s) <= pship.range();
			if (mode==local && !starInRange) continue;
			const float	size=s.size*(sizefactor/df2) + 7,
						jx=cx+((distfactor/df2)*s.x) - (mode==local ? psys.x*(distfactor/df2) : 0), // center on the player for local mode
						jy=cy+((distfactor/df2)*s.y) - (mode==local ? psys.y*(distfactor/df2) : 0),
						px=jx-(size/2),
						py=jy-(size/2);
			
			{ // now we actually get around to drawing the star.

				/*draw glow*/
				glEnable(GL_BLEND);
				const color::color& cl = data::system::types[(size_t)s.type()].color; //evil? try "efficient"
				draw::circleGrad(jx, jy, size/2, cl,1, cl, 0, .4);

				/* draw the center */
				glEnable(GL_MULTISAMPLE);
				glColor3f(1,1,1);
				draw::circle(jx, jy, size/5, .4, true);
				glDisable(GL_MULTISAMPLE);
				glDisable(GL_BLEND);
			}

			if (b_show==&b_show_factions) {
				if (s.faction!=nullptr) {
					glEnable(GL_BLEND);
					glEnable(GL_MULTISAMPLE);
					glBegin(GL_QUADS);
						color::apply(*s.faction->color, (hlt==&s ? hover : off), color::colortype::bg,0);
							glVertex2f(jx,jy);
							glVertex2f(jx+10,jy+10);
						color::apply(*s.faction->color, (hlt==&s ? hover : off), color::colortype::bg);
							glVertex2f(jx,jy+20);
						color::apply(*s.faction->color, (hlt==&s ? hover : off), color::colortype::bg,0);
							glVertex2f(jx-10,jy+10);
					glEnd();
					glDisable(GL_MULTISAMPLE);
					glDisable(GL_BLEND);
				}
			} else if (b_show==&b_show_informants) {
				// check for informants
			}
		}
		{ /* draw homing circles wherever the player is */
			const float	size=psys.size*(sizefactor/df2) + 7,
						jx=cx+(mode==local ? 0 : psys.x*distfactor),
						jy=cy+(mode==local ? 0 : psys.y*distfactor),
						px=jx-(size/2),
						py=jy-(size/2);
			glEnable(GL_BLEND);
			glEnable(GL_MULTISAMPLE);
			// draw hyperdrive range
			glColor4f(1,.6,.6,1);
			draw::circle(jx,jy,size+10,.7);
			draw::circle(jx,jy,pship.range()*(distfactor/df2),1.3);
			glDisable(GL_BLEND);
			glDisable(GL_MULTISAMPLE);
		}
		
		if (hilitesys || hlt) {
			data::system& s = hlt ? *((data::system*)hlt) : *((data::system*)hilitesys);
			const bool starInRange = pship.canJumpTo(&s);
			const float	size=s.size*(sizefactor/df2) + 7,
						jx=cx+((distfactor/df2)*s.x) - (mode==local ? psys.x*(distfactor/df2) : 0),
						jy=cy+((distfactor/df2)*s.y) - (mode==local ? psys.y*(distfactor/df2) : 0),
						px=jx-(size/2),
						py=jy-(size/2);
			const float w = Font::ft_zekton.BBox(s.name.c_str()).Upper().X(),
						pad = (w/2)+10;
			// draw other crap
			// is the player here?
			glEnable(GL_BLEND);
			glEnable(GL_MULTISAMPLE);
			if(hlt==&s || hilitesys==&s) {
				// draw concentric circles
				static float cpos[4] = {0,.5,1,1.5};
				for (int j=0;j<4;j++) {
					glColor4f(1,1,1,(2.0f - cpos[j])/2);
					draw::circle(jx,jy,cpos[j]*size, 1);
					cpos[j]+=window::ticks();
					while (cpos[j]>2) cpos[j]-=2; // worst. modulo. ever. floats suck.
				}
				// draw routing line
				if (starInRange)	glColor4f(1,1,1,.5);
							else	glColor4f(1,.2,.2,.4);
				
				glBegin(GL_LINES);
					glVertex2f(jx,jy);
					if (mode==local) //cheat a bit
						glVertex2f(window::width/2,window::height/2 + 35);
					else
						glVertex2f(cx+psys.x*distfactor, cy+psys.y*distfactor);
				glEnd();
			}
						
			// draw caption
			glBegin(GL_QUADS);
				glColor4f(.4,0,.3,.8);
					glVertex2f(jx-pad,py-38.5);
					glVertex2f(jx+pad,py-38.5);
				glColor4f(.5,.1,.4,.9);
					glVertex2f(jx+pad,py-5.5);
					glVertex2f(jx-pad,py-5.5);
			glEnd();
			
			glColor4f(1,1,1,1);
			Font::ft_zekton.FaceSize(18);
			glRasterPos2f(jx-(w/2),py-30);

			glDisable(GL_MULTISAMPLE); glDisable(GL_BLEND);
			Font::ft_zekton.Render(s.name.c_str());
			glDisable(GL_MULTISAMPLE); glDisable(GL_BLEND);
			
			glColor4f(.6,.2,.5,1);
			glBegin(GL_LINE_LOOP)
			; glVertex2f(jx-pad,py-38.5)
			; glVertex2f(jx+pad,py-38.5)
			; glVertex2f(jx+pad,py-5.5)
			; glVertex2f(jx-pad,py-5.5);
			glEnd();// yep, I'm fucking with you.
			glDisable(GL_MULTISAMPLE);
			glDisable(GL_BLEND);
		}
		
		Controller::render();
		{
			glEnable(GL_BLEND);
			glEnable(GL_MULTISAMPLE);
			float	cp = window::width / 2,
					tw = date.width() / 4;
			glColor4f(0,0,0,.8);
			glBegin(GL_QUADS);
				glVertex2f(cp - tw, window::height);
				glVertex2f(cp + tw, window::height);
				glVertex2f(cp + tw - 10, window::height-25);
				glVertex2f(cp - tw + 10, window::height-25);
			glEnd();
			glColor4f(.7,.8,1,1);
			glBegin(GL_LINE_LOOP);
				glVertex2f(cp - tw, window::height);
				glVertex2f(cp + tw, window::height);
				glVertex2f(cp + tw - 10, window::height-25);
				glVertex2f(cp - tw + 10, window::height-25);
				glVertex2f(cp - tw, window::height);
			glEnd();
			glColor3f(1,1,1);
			date.render(0,window::height - 17);
			glDisable(GL_BLEND);
			glDisable(GL_MULTISAMPLE);
		}
		
		if (dialog!=nullptr) {
			// dim the background
			glEnable(GL_BLEND);
			glColor4f(0,0,0,.6);
			glBegin(GL_QUADS);
				glVertex2f(0,				0);
				glVertex2f(0,				window::height);
				glVertex2f(window::width,	window::height);
				glVertex2f(window::width,	0);
			glEnd();
			glDisable(GL_BLEND);
			
			dialog->render();
		}
	};
	void Map::loop() {
		
	}
	void Map::hilite(data::system* s) {
		hilitesys=s;

		data::ship& pship = *data::player->getShip();
		if (pship.distanceTo(s) <= pship.range())
			mode = local, b_galactic_view.toggle=ui::Button::togglestate::toggle; 
		else
			mode = galactic, b_galactic_view.toggle=ui::Button::togglestate::on;
		
		b_find_system.toggle=ui::Button::togglestate::on;
	}
	void Map::reactivate(void* v) {
		update();
	}
	void Map::update() {
		cursys = data::player->getSystem(); // figure out where the player is
		sysname.str=cursys->name;
		date.str = strings::date(data::utime);
		if (cursys->faction != nullptr) facname.str=cursys->faction->name;
		
		if (data::player->loc.type==data::location_t::Type::ship) {
			data::ship& s = *(data::ship*)data::player->loc.loc;
			shipname.str=s.name;
			shipname.x=(int)(window::width - (shipname.width()+20)); //round it so the text doesn't look hideous
			shipname.y=window::height - 46;
			
			shipclass.str=std::string(s.shipclass->name);
			shipclass.x=(int)(window::width - (shipclass.width()+20));
			shipclass.y=window::height - (46+12+18);
		}
	}
	void Map::handle(const ui::event& e) {
		if (dialog!=nullptr) {
			if (e.type==ui::eventtype::key && e.key.state==ui::pressed && e.key.key==GLFW_KEY_ESC)
				dialog=nullptr;
			else
				dialog->handle(e);
		} else {
			if (e.type==ui::eventtype::motion) {
				data::system& psys = *cursys; // figure out where the player is
				data::ship& pship = *data::player->getShip();
				float df2;
				if (mode==local) {
					df2=(pship.range()*distfactor) / (radius/2); //scale back up. this is heinous. FIXME FIXME FIXME
				} else df2=1;
			
				const float cx = window::width / 2, cy = window::height / 2 + 35;
				for (int i=0;i<data::systemc;i++) { const data::system& s = data::systems[i];
					//make sure we're in range before we bother with the math
					if (mode == local && !(pship.distanceTo(&s) <= pship.range())) continue;
					const float	size=s.size*(sizefactor/df2)+7,
								px = (cx+((distfactor/df2)*s.x)-(size/2))-(mode==local ? psys.x*(distfactor/df2) : 0),
								py = (cy+((distfactor/df2)*s.y)-(size/2))-(mode==local ? psys.y*(distfactor/df2) : 0);
					if (e.motion.x>=px && e.motion.x <= px+size && e.motion.y >= py && e.motion.y <= py+size) {
						hlt=(void*)&s;
						return;
					}
				}
				hlt=nullptr;
			} else if (e.type==ui::eventtype::click && e.click.state==ui::released && hlt!=nullptr) {
				if (hlt==cursys)	window::load(new SystemMap((data::system*)cursys));
							else	window::load(new Scan((data::system*)hlt));
			} else if (e.type==ui::eventtype::key && e.key.state==ui::released) {
				// can't be ui::pressed or hijinks will ensue
				switch (e.key.key) {
					case 'V':	if (mode==local)
									mode = galactic, b_galactic_view.toggle=ui::Button::togglestate::on; 
								else
									mode = local, b_galactic_view.toggle=ui::Button::togglestate::toggle;
								break;
					case 'I': b_show=&b_show_informants; break;
					case 'F': b_show=&b_show_factions; break;
					case 'N': b_show=&b_show_none; break;
					case ' ': window::load(new SystemMap((data::system*)cursys)); break;
					case 'S': callback(&b_find_system); break;
				}
			}
			Controller::handle(e);
		}
	}
	void Map::show(ui::Group* _dialog) {
		
		dialog = _dialog;
		// call the loop() method, which in this case doesn't loop anything and merely resets the form.
		dialog->loop(); // this is one of the worst things I've ever done. You're allowed to hate me.
	}
	void Map::callback(void* w) {
		if (w==&b_galactic_view) {
			if (mode==local) mode = galactic; else mode = local;
		} else if (w==&b_find_system) {
			if (hilitesys) {
				b_find_system.toggle = ui::Button::togglestate::none;
				hilitesys=nullptr;
			} else show(&find);
		}
	}
	
	// DIALOGS
	
	Map::FindDialog::FindDialog() :
		search("Search", 205,0,195,60, &color::green),
		cancel("Cancel", 0,0,195,60, &color::purple),
		name(0,70,400,25)
	{
		parent=nullptr;
		x=(window::width-400)/2;
		y=(window::height-148)/2;
		add(&search);
		add(&cancel);
		add(&name);
	};
	void Map::FindDialog::loop() {
		name.setText("");
		focusOn(&name);
	}
	void Map::FindDialog::callback(void *w) {
		if (w==&search || (w==&name && focuswidget==nullptr)) {
		                  // Has RETURN been pressed while the text field was focused? Yes, this is lazy and horrible.
			for (unsigned int i=0; i<data::systemc; i++) {
				if (stricmp(data::systems[i].name,name.text)) {
					window::alert(std::string("Found ")+data::systems[i].name+".");
					((Map*)window::controller) -> hilite(&data::systems[i]);
					goto close;
				}
			}
			// if we've reached this point, nothing has been found.
			window::alert("No system found with that name.",&color::red);
			name.setText("");
			focusOn(&name);
		} else if (w==&cancel) {
			goto close; // yeah, yeah
		}
		return;
		close:	((Map*)window::controller) -> dialog = nullptr; //haaaack - this is my fault for using singletons.
																//THEY NEVER DON'T MAKE THINGS WORSE
		
	}
}