#!/bin/bash

CC="clang -g -std=c++11 \
	-fno-rtti	\
	-fshort-enums"
	
if [ `uname` == "Linux" ]
then	# Linux build
	FREETYPE=`freetype-config --cflags`
	FREETYPELD=`freetype-config --libs`
	CC="$CC -I$(pwd)/lib $FREETYPE"
	LDFLAGS="-lstdc++ -lftgl -lm -lGL -lX11 -lXrandr -pthreads -Llib -lglfw-lin"
	echo -e "\033[35mBuilding for \033[1mLinux\033[0m"

else	# Mac build
	FREETYPE=`lib/freetype/bin/freetype-config --prefix=\`pwd\`/lib/freetype --cflags`
	FREETYPELD=`lib/freetype/bin/freetype-config --prefix=\`pwd\`/lib/freetype --libs`
	CC="$CC -DOS_MAC -g -I$(pwd)/lib -I$(pwd)/lib/ftgl/include $FREETYPE"

	LDFLAGS="	-lstdc++		\
			-framework OpenGL	\
			-framework Cocoa	\
			-framework IOKit	\
			-Llib			\
				-lglfw-mac	\
			-Llib/ftgl/lib	\
				-lftgl 		\
			-Wl,-rpath,lib"
	echo -e "\033[35mBuilding for \033[1mMac OS X\033[0m"
fi

LDFLAGS="$LDFLAGS $FREETYPELD"
OUTPUT=${PWD##*/}
SRC=src

justlink=0
task=buildall
case $1 in
	link  ) justlink=1;;
	clean ) task=cleanall;;
	cc    ) cd $SRC
				cd `dirname $2`
				echo -e "$indent\033[1;33m  CC\033[0;1m  $2\033[0m"
				$CC -c `basename $2`
			exit;;
esac

buildall() {
	for file in *.cc
	do
		echo -e "$indent\033[1;33m  CC\033[0;1m  $file\033[0m"
		$CC -c $file &
	done
}
cleanall() {
	for file in *.o
	do
		echo -e "$indent\033[1;31m  RM\033[0;1m  $file\033[0m"
		rm $file
	done
}
cd src
	if [ "$justlink" == "0" ]
		then $task
	fi
	indent="\t"
	
	for dir in controllers ui data
	do
		echo -e "\033[32;1m   >>\033[0;1m $dir\033[0m"
		cd $dir
		if [ "$justlink" == "0" ]
			then $task
		fi
		srcdirs="$srcdirs $SRC/$dir/*.o"
		cd ..
	done

cd ..

if [ "$task" != "cleanall" ]
then
	wait
	echo -e "\033[1;36m LINK\033[0;1m  $OUTPUT\033[0m"
	$CC $SRC/*.o $srcdirs $LDFLAGS -o $OUTPUT
fi
