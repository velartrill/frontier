#include "color.h"
#include "alert.h"
#include <glfw.h>
#include <list>
#include "window.h"
#include "text.h"
#include "font.h"
#include "util.h"
#include "ui.h"
namespace window {
	namespace {
		struct alert_t { std::string text; color::scheme* color; float time; };
		std::list<alert_t> alerts;
		FTSimpleLayout alert_text;
	}
	void alert(std::string msg, color::scheme* color) {
		window::curAlert=true;
		alerts.push_back({msg, color, 3.0f});
	}
	void renderAlert() {
		float cursor = 30;
		alert_text.SetFont(&Font::ft_zekton);
		Font::ft_zekton.FaceSize(20.0f);
		alert_text.SetLineLength(window::width-20);
		alert_text.SetAlignment((FTGL::TextAlignment)Font::align::center);
		glEnable(GL_BLEND);
		for (alert_t& alert : alerts) {
			float top = window::height-cursor;
			if (alerts.front().time<1) top += 22.0f * (1-alerts.front().time);

			float alpha = (alert.time < 1 ? alert.time : 1);
			float textheight = alert_text.BBox(alert.text.c_str()).Upper().Y() - alert_text.BBox(alert.text.c_str()).Lower().Y();
			color::apply(*alert.color, ui::Widget::off, color::colortype::bg,alpha<.4?0:alpha-.4);
			glBegin(GL_QUADS);
				glVertex2f(0,top+10);
				glVertex2f(window::width,top+10);
			color::apply(*alert.color, ui::Widget::off, color::colortype::bg,alpha<.2?0:alpha-.2);
				glVertex2f(window::width,top-(textheight+10));
				glVertex2f(0,top-(textheight+10));
			glEnd();
			color::apply(*alert.color, ui::Widget::off, color::colortype::fg,alpha);
			glRasterPos2f(10, top-17);
			alert_text.Render(alert.text.c_str());
			cursor += textheight+22;
		}
		glDisable(GL_BLEND);
		alerts.front().time-=window::ticks();		// operate only on the front so user has time to read others
		if (alerts.front().time <= 0) {
			alerts.pop_front();
			if (alerts.empty()) curAlert=false;
		}
	}
}