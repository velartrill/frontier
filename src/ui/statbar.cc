#include "statbar.h"
#include <glfw.h>
#include "../util.h"
#define CORNER 15
#include "../window.h"
#include "../tostring.h"
namespace {
	void drawBase(float x, float y, float w, float h) {
		float corner;
		if (w<=0) return;
		if (w>CORNER) corner=CORNER; else corner=w, h-=w;
		if (h<corner) w-=(corner-h)*2, x+=(corner-h), corner=h;
		glBegin(GL_POLYGON);
			glVertex2f(x,y+corner);
			glVertex2f(x+corner,y);
			glVertex2f(x+w,y);
			glVertex2f(x+w,y+h-corner);
			glVertex2f(x+w-corner,y+h);
			glVertex2f(x,y+h);
		glEnd();
	}
}
namespace ui {
	StatBar::StatBar(float _w, float _h, float _max) : tooltipText("") {
		w=_w, h=_h, max=_max;
		color=&color::blue;
		hoverseries=nullptr;
	}
	StatBar::StatBar(float _x, float _y, float _w, float _h, float _max) : tooltipText("") {
		x=_x, y=_y, w=_w, h=_h, max=_max;
		color=&color::blue;
		hoverseries=nullptr;
	}
	void StatBar::render() {
		float x=abx(), y=aby();
		glEnable(GL_BLEND);
		glEnable(GL_MULTISAMPLE);
			color::apply(*color, active, color::colortype::border);
			drawBase(x,y,w,h);
			color::apply(*color, active, color::colortype::bg);
			drawBase(x+1,y+1,w-2,h-2);

			for (unsigned char i=0;i<seriesc;i++) {
				color::apply(*series[i].color, off, color::colortype::border);
				float sw = w * (series[i].val / max);
				drawBase(x+4,y+4,sw-8,h-8);
				color::apply(*series[i].color, off, color::colortype::bg);
				drawBase(x+5,y+5,sw-10,h-10);
			}
		if (state==hover && hoverseries!=nullptr) {
			float sw = tooltipText.width(), sx = window::mousex-(sw/2), sy=y+h+10;

		//there has got to be a better way to do this, but I'm too tired to be arsed to work it out.
			if (sx+sw>window::width-15) sx -= ((sx+sw) - (window::width-15)); 
			if (sx<15) sx += 15-sx;
			
			color::apply(*hoverseries->color, off, color::colortype::border);
			glBegin(GL_LINE_STRIP);
				glVertex2f(sx-10,sy+26);
				glVertex2f(sx+10+sw,sy+26);
				glVertex2f(sx+10+sw,sy-8);
				glVertex2f(sx-10,sy-8);
				glVertex2f(sx-10,sy+26);
			glEnd();
		glDisable(GL_MULTISAMPLE);
			color::apply(*hoverseries->color, off, color::colortype::bg, .3);
			glBegin(GL_QUADS);
				glVertex2f(sx-10,sy+26);
				glVertex2f(sx+10+sw,sy+26);
			color::apply(*hoverseries->color, off, color::colortype::bg,.6);
				glVertex2f(sx+10+sw,sy-8);
				glVertex2f(sx-10,sy-8);
			glEnd();
			color::apply(*hoverseries->color, off, color::colortype::fg);
			tooltipText.render(sx, sy);
		} else glDisable(GL_MULTISAMPLE);
		glDisable(GL_BLEND);
	}
	void StatBar::loop() {};
	void StatBar::handle(const ui::event& e) {
		if ((e.type==ui::eventtype::click && e.click.state==pressed) || (e.type==ui::eventtype::motion && state==active)) {
			lastClick = max * ((window::mousex - abx()) / w); //<del>this formula seems suspiciously simple...</del> that was why...
			parent->callback(this);
		} else if (e.type==ui::eventtype::motion) {
			float px = abx(), py = aby();
			for (unsigned char i=seriesc; i!=0; i--) {
				float sw = w * (series[i-1].val / max) + px; //really? I forgot the px originally? Nice job, Alex!
				if (e.motion.x <= sw) {
					hoverseries=&series[i-1];
					tooltipText.str=std::string(hoverseries->name) + ": " + toString(series[i-1].val) + "/" + toString(max);
					break;
				}
			}
		}
	};
	bool StatBar::inhitbox(float px, float py) {
		return (px>=abx() && px<=abx()+w && py>=aby() && py<=aby()+h);
	}
	
}