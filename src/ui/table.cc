#include "table.h"
#include "../font.h"
namespace ui {
	Table::Table(float _x, float _y, float _w, float _h, float _lcolw, float _size, color::scheme* _color) {
		x=_x, y=_y, w=_w, h=_h, lcolw=_lcolw, size=_size, color=_color;
	};
	void Table::loop() {}
	void Table::handle(const event&) {}
	void Table::render() {
		float cursor=0; const float px = abx(), py=aby();
		FR_FTGL_FONT_TYPE& f = Font::ft_zekton;
		f.FaceSize(size);
		for (row& r : rows) {
			glColor4f(r.textcolor.r, r.textcolor.g, r.textcolor.b,.75f);
			glRasterPos2f(px, py+cursor);
			f.Render(r.name);
			
			glColor3f(r.namecolor.r, r.namecolor.g, r.namecolor.b);
			glRasterPos2f(px+lcolw, py+cursor);
			f.Render(r.text.c_str());
			
			cursor-=3;
			
			glEnable(GL_BLEND);
			glBegin(GL_LINE_STRIP);
			color::apply(*color, ui::Widget::off, color::colortype::border, 0.0f);
				glVertex2f(px,py+cursor);
			color::apply(*color, ui::Widget::off, color::colortype::border, 0.7f);
				glVertex2f(px+30,py+cursor);
				glVertex2f(px+w-30,py+cursor);
			color::apply(*color, ui::Widget::off, color::colortype::border, 0.0f);
				glVertex2f(px+w,py+cursor);
			glEnd();
			cursor-=size+5;
			glDisable(GL_BLEND);
		}
	}
	void Table::addrow(const char* label, std::string text) {
		rows.push_back({label,{1,1,1},text,{1,1,1}});
	}
	void Table::addrow(const char* label, std::string text, color::color color) {
		rows.push_back({label,color,text,color});
	}
	void Table::addrow(const char* label, std::string text, color::color lcolor, color::color vcolor) {
		rows.push_back({label,lcolor,text,vcolor});
	}
}