#include "image.h"
#include <cstring>
#include <glfw.h>
#include <iostream>
Image::Image(const char* name, float _w, float _h, bool _tint) {
	int fullname=strlen(name) + 1 + 12 + 1;
	char n[fullname]; n[0]=0;
	strcat(n, "assets/img/");
	strcat(n, name);
	
	glGenTextures(1, &_handle);
	glBindTexture(GL_TEXTURE_2D, _handle);
	std::cout<<"\x1b[34;1m[ DD ]\x1b[0m Loading texture "<<n<<'\n';
	if (!glfwLoadTexture2D(n, GLFW_BUILD_MIPMAPS_BIT)) throw "Cannot load texture";
	// note to self:  GL can't handle images without mipmaps. Don't waste half an hour
	//                figuring that out next time.
	w		=	_w;
	h		=	_h;
	tint	=	_tint;
}
Image::~Image() {
	glDeleteTextures(1, &_handle);
}
void Image::render(float x, float y) {
	if (!tint) glColor3f(1,1,1);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, _handle);
		glBegin(GL_QUADS);
			glTexCoord2f(0,0);	glVertex2f(x,y);
			glTexCoord2f(1,0);	glVertex2f(x+w,y);
			glTexCoord2f(1,1);	glVertex2f(x+w,y+h);
			glTexCoord2f(0,1);	glVertex2f(x,y+h);
		glEnd();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}
void Image::render(float x, float y, float w, float h) {	// yeah, this is hacky
	if (!tint) glColor3f(1,1,1);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, _handle);
		glBegin(GL_QUADS);
			glTexCoord2f(0,0);		glVertex2f(x,y);
			glTexCoord2f(1,0);		glVertex2f(x+w,y);
			glTexCoord2f(1,1);		glVertex2f(x+w,y+h);
			glTexCoord2f(0,1);		glVertex2f(x,y+h);
		glEnd();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}