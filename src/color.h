#pragma once
#include "ui.h"
#include <glfw.h>

namespace color {
	struct color {
		float r, g, b;
	};
	struct colorpair {
		color bg, fg, border;
	};
	struct scheme {	// we need to go deeper!
		colorpair base, hover, active, focus;
	};
	enum class colortype { bg, fg, border };
	extern scheme blue, red, brown, green, purple;

	void apply(const scheme& c, ui::Widget::State state, colortype type, float alpha = 1.0f);
}