#include "text.h"
#include <FTGL/ftgl.h>
#include <glfw.h>
#include "util.h"

//Note to self:	Don't do this next time. Seriously. Cut this shit out. You had it right
//              the first time. Just write a damn function to print text. Ignore the
//              blandishments of OOP. Nothing good will come of heeding them.

namespace Font {
	FR_FTGL_FONT_TYPE	ft_header("assets/font/header.ttf"),
						ft_body("assets/font/body.ttf"),
						ft_zekton("assets/font/zekton.ttf"),
						ft_title("assets/font/sofachro.ttf");
}

Text::Text(std::string _str, Font::face _font, float _size) {
	font=_font;
	str=_str;
	size=_size;
}
void Text::render(float x, float y) {
	FR_FTGL_FONT_TYPE& f =	(	font == Font::face::header? Font::ft_header	:
								font == Font::face::body	? Font::ft_body	:
								font == Font::face::zekton? Font::ft_zekton	:
							/*	font == Font::face::title ?*/ Font::ft_title	);
	/* glPushMatrix();
	glTranslatef(x,y,0); */
	glRasterPos2f(x,y);
	f.FaceSize(size);
	f.Render(str.c_str(),str.size());
	// glPopMatrix();
}

TextBox::TextBox(std::string _str, float w, Font::align _align, Font::face _font, float _size) {
	size=_size;
	font =	(	_font == Font::face::header? &Font::ft_header	:
				_font == Font::face::body	? &Font::ft_body	:
				_font == Font::face::zekton? &Font::ft_zekton	:
			/*	_font == Font::face::title ?*/ &Font::ft_title	);
	box.SetFont(font);
	box.SetLineLength(w);
	align(_align);
	str=_str;
}

void TextBox::render(float x, float y) {
	// glPushMatrix();
// 	glTranslatef(x,y,0);
	font->FaceSize(size);
	glRasterPos2f(x,y);
	box.Render(str.c_str(),str.size());
	// glPopMatrix();
}
float TextBox::width() {
	return box.BBox(str.c_str()).Upper().X();
}
float TextBox::height() {
	return box.BBox(str.c_str()).Lower().Y();
}
float Text::width() {		//arrrrghhh hack hack hack FIXME
	FR_FTGL_FONT_TYPE& fn = (	font == Font::face::header? Font::ft_header	:
				font == Font::face::body	? Font::ft_body	:
				font == Font::face::zekton? Font::ft_zekton	:
			/*	font == Font::face::title ?*/ Font::ft_title	);
	fn.FaceSize(size);
	return fn.BBox(str.c_str()).Upper().X();
}