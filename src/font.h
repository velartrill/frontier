#pragma once
#include <FTGL/ftgl.h>
// used for raw FTGL access
// avoid where possible
#define FR_FTGL_FONT_TYPE FTGLPixmapFont//  FTGLTextureFont
namespace Font {
	extern FR_FTGL_FONT_TYPE	ft_header,
								ft_body,
								ft_zekton,
								ft_title;
}