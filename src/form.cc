#include "form.h"
#include "window.h"
#include <glfw.h>
#include "util.h"
namespace ui {
	Form::Form() {
		cursor=window::height;
		color=&color::purple;
	}
	void Form::add_with_label(std::string label, Widget* w, float height) {
		cursor -= 20+height;
		labels.push_back(new Label(label, 40, cursor+((height-20)/2), 200)); // I hate math.
		labels.back()->parent=this;
		labels.back()->text.align(Font::align::right);
		widgets.push_back(labels.back());
		
		w->x=260; w->y = cursor;
		w->parent=this;
		widgets.push_back(w);
	}
	Label* Form::addtitle(std::string text){
		Label* l = new Label(text, 20, cursor-65, window::width-40, 52, {1,1,1}, Font::face::zekton);
		l->parent=this;
		l->shadow=true;
		labels.push_back(l);
		widgets.push_back(l);
		cursor-=90;
		return l;
	}
	void Form::render() {
		glEnable(GL_BLEND);
		//title gradient
			glBegin(GL_QUADS);
			color::apply(*color, off, color::colortype::bg, .5);
				glVertex2f(0,window::height);
				glVertex2f(window::width,window::height);
			color::apply(*color, off, color::colortype::bg, .3);
				glVertex2f(window::width,window::height-92);
				glVertex2f(0,window::height-92);
			glEnd();
		//border line
			color::apply(*color, off, color::colortype::border);
			glBegin(GL_LINES);
				glVertex2f(0,window::height-92);
				glVertex2f(window::width,window::height-92);
			glEnd();
		//background gradient
			glBegin(GL_QUADS);
			color::apply(*color, off, color::colortype::bg, .3);
				glVertex2f(0,window::height);
				glVertex2f(window::width,window::height);
			color::apply(*color, off, color::colortype::bg, .1);
				glVertex2f(window::width,0);
				glVertex2f(0,0);
			glEnd();
		glDisable(GL_BLEND);
		Controller::render();
	}
	Form::~Form() {
		for (Label* l : labels) delete l;
	}
}