#include "title.h"
#include "../text.h"
#include <glfw.h>
#include "../window.h"
#include "../delegate.h"
#include "../data/universe.h"
#include "map.h"
#include "../alert.h"
//
//	Title
//
Controllers::Title::Title() :
	b_newgame("New Game",window::width-320,20,300,80),
	b_quit("Quit Game",20,20,300,80),
	logo("logo.tga",1024,512)
{
	add(&b_newgame);
	add(&b_quit);
}

void Controllers::Title::render() {
	logo.render((window::width - 1024) / 2, (window::height - 512) / 2);
	Controller::render();
};
void Controllers::Title::callback(void* w) {
	if (w==&b_newgame) {
		window::load(new NewGame);
	} else if (w==&b_quit) {
		glfwTerminate();
	}
};

//
//	NewGame
//
namespace Controllers {
	#define ENDPOINT(wd) wd.x + wd.w
	
	Title::NewGame::NewGame() :
		i_name(300,20),
		i_ship(300,20),
		b_gender_female("Female",140,80),
		b_gender_nonbinary("Non-Binary",140,80),
		b_gender_male("Male",140,80),
		b_next("Next",window::width-220, 20, 200, 80, &color::green)
	{
		addtitle("New Game");
		add_with_label("Character Name:",&i_name,48);
		b_gender_nonbinary.group=&b_gender;
		b_gender_female.group=&b_gender;
		b_gender_male.group=&b_gender;
		b_gender=nullptr;
		add_with_label("Pronouns:",&b_gender_nonbinary);
		b_gender_female.x=ENDPOINT(b_gender_nonbinary)+20;
		b_gender_female.y=b_gender_nonbinary.y;
		add(&b_gender_female);
		b_gender_male.x=ENDPOINT(b_gender_female)+20;
		b_gender_male.y=b_gender_female.y;
		add(&b_gender_male);
		cursor-=20;
		add_with_label("Ship Name:",&i_ship,48);
		add(&b_next);

		i_name.takeFocus();
	}
	void Title::NewGame::callback(void* w) {
		if (w==&b_next) {
			if (i_name.text.empty()) 		window::alert("Name your character.",&color::red);
			else if (b_gender==nullptr)		window::alert("Select a set of pronouns.",&color::red);
			else if (i_ship.text.empty()) 	window::alert("Name your ship.",&color::red);
			else {
				using data::player;
				player=new data::character;
				player->name = i_name.text;
				if (b_gender == &b_gender_female)
					player->pronoun=data::character::female;
				else if (b_gender == &b_gender_nonbinary)
					player->pronoun=data::character::nonbinary;
				else
					player->pronoun=data::character::male;
				data::generateUniverse(i_ship.text); //haaaack
				window::load(new Map, false);
			}
		}
	}
	/* DEBUG CODE - Speed up getting past the first screen. REMOVE in production */
		void Title::NewGame::auxcallback(void* w) {
			if (w==&b_next) {
				i_name.setText("Alexis");
				i_ship.setText("Profit Motive");
				b_gender=&b_gender_female;
				callback(w);
			}
		}
	/* END DEBUG CODE */
}