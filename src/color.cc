#include "color.h"
namespace color {
	scheme blue = {
		//	bg				fg			border
		{{0.1,0.2,0.4},	{0.5,0.7,0.8}, {0.3,0.4,0.6}},	// off
		{{0.5,0.7,0.8},	{0.2,0.3,0.5}, {1.0,1.0,1.0}},	// active
		{{0.1,0.2,0.3},	{0.6,0.7,0.9}, {0.3,0.5,0.7}},	// down
		{{0.0,0.1,0.2},	{0.6,0.9,1.0}, {0.4,0.6,0.8}}	// focus
	};
	scheme brown = {
		{{0.5,0.3,0.2},	{0.8,0.7,0.6}, {0.7,0.5,0.4}},
		{{1.0,0.8,0.7},	{0.5,0.3,0.2}, {0.5,0.3,0.2}},
		{{0.3,0.2,0.1},	{0.9,0.7,0.6}, {0.9,0.7,0.6}},
		{{0.3,0.2,0.1},	{0.9,0.7,0.6}, {0.9,0.7,0.6}}
	};                              
	scheme red = {                 
		{{0.5,0.2,0.2},	{0.8,0.6,0.6}, {0.7,0.4,0.4}},
		{{1.0,0.7,0.7},	{0.7,0.3,0.3}, {0.8,0.5,0.5}},
		{{0.3,0.1,0.1},	{0.9,0.5,0.5}, {0.6,0.3,0.3}},
		{{0.3,0.1,0.1},	{0.9,0.5,0.5}, {0.9,0.6,0.6}}
	};                              
	scheme green = {                
		{{0.2,0.5,0.3},	{0.6,1.0,0.8}, {0.4,0.7,0.5}},
		{{1.0,1.0,1.0},	{0.2,0.5,0.3}, {0.5,0.8,0.6}},
		{{0.1,0.3,0.2},	{0.6,0.9,0.7}, {0.3,0.6,0.4}},
		{{0.1,0.3,0.2},	{0.6,0.9,0.7}, {0.6,0.9,0.7}}
	};                              
	scheme purple = {               
		{{0.2,0.0,0.5},	{0.8,0.4,1.0}, {0.8,0.4,1.0}}, 
		{{0.8,0.4,1},	{0.2,0.0,0.5}, {0.2,0.0,0.5}},
		{{0.1,0.0,0.3},	{0.5,0.1,0.7}, {0.5,0.1,0.7}},
		{{0.1,0.0,0.3},	{0.5,0.1,0.7}, {0.5,0.1,0.7}}
	};
	void apply(const scheme& c, ui::Widget::State state, colortype type, float alpha) {
		switch (type) {
			case colortype::fg:
				switch (state) {
					case ui::Widget::off:		glColor4f(c.base.fg.r,c.base.fg.g,c.base.fg.b,alpha); break;
					case ui::Widget::hover:		glColor4f(c.hover.fg.r,c.hover.fg.g,c.hover.fg.b,alpha); break;
					case ui::Widget::active:	glColor4f(c.active.fg.r,c.active.fg.g,c.active.fg.b,alpha); break;
					case ui::Widget::focus:		glColor4f(c.focus.fg.r,c.focus.fg.g,c.focus.fg.b,alpha); break;
				}
			break;
			case colortype::bg:
				switch (state) {
					case ui::Widget::off:		glColor4f(c.base.bg.r,c.base.bg.g,c.base.bg.b, alpha); break;
					case ui::Widget::hover:		glColor4f(c.hover.bg.r,c.hover.bg.g,c.hover.bg.b, alpha); break;
					case ui::Widget::active:	glColor4f(c.active.bg.r,c.active.bg.g,c.active.bg.b, alpha); break;
					case ui::Widget::focus:		glColor4f(c.focus.bg.r,c.focus.bg.g,c.focus.bg.b, alpha); break;
				}
			break;
			case colortype::border:
				switch (state) {
					case ui::Widget::off:		glColor4f(c.base.border.r,c.base.border.g,c.base.border.b, alpha); break;
					case ui::Widget::hover:		glColor4f(c.hover.border.r,c.hover.border.g,c.hover.border.b, alpha); break;
					case ui::Widget::active:	glColor4f(c.active.border.r,c.active.border.g,c.active.border.b, alpha); break;
					case ui::Widget::focus:		glColor4f(c.focus.border.r,c.focus.border.g,c.focus.border.b, alpha); break;
				}
			break;
		}
	}
}