#pragma once
namespace data {
	/* Ships */
	typedef	float			dist_t;	// interstellar distances
	typedef float			orbit_t; // interplanetary distances
	typedef unsigned int	health_t; // hull strength, energy, etc.
	
	/* Economy */
	typedef unsigned long long int	balance_t; // monetary sums
	typedef	long int				price_t; // prices
	typedef unsigned char			tech_t; // tech level ~ 0..20
	typedef unsigned char			rarity_t; // rarity
	typedef signed long int			quantity_t; // quantities
	
	/* Universe */
	typedef	unsigned int	utime_t; // universal timestamp (# of days since start of game)
	typedef unsigned long	year_t; // year identifier
	typedef unsigned char	month_t;
	typedef unsigned char	day_t;
	typedef unsigned int	weight_t; // weights
}