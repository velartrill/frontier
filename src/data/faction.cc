#include "faction.h"
#include <vector>
namespace data {
	extern ship::Class	aegis_shipclasses[],
						empire_shipclasses[],
						society_shipclasses[];
	extern item	aegis_items[],
				empire_items[],
				society_items[];
	extern const char*  aegis_syllables[],
                     *  empire_suffixes[],
                     *  empire_syllables[],
	                 *  society_syllables[];
	Faction factions[] = {
		{"Aegis","",&color::purple,"aegis.tga",		6,aegis_shipclasses,	7,	aegis_items, Faction::Ideology::communist,	20,	.6,
			{71,aegis_syllables,	0,nullptr,			false,true,true,false,		2,2, 3}},
						
		{"Society","",&color::green, "society.tga",	7,society_shipclasses,	1,	society_items, Faction::Ideology::socialist,	13,	1,
			{51,society_syllables,	0,nullptr,			false,false,false,false,	2,2, 2}},

		{"Empire","",&color::brown, "empire.tga",	4,empire_shipclasses,	1, empire_items, Faction::Ideology::consumerist,	7,	.2,
			{92,empire_syllables,	36,empire_suffixes,	false,false,false,true,		1,2, 2}},
						
	};
	const unsigned char factionc = 3;
	
	//
	// Member functions
	//
	namespace {
		bool isVowel(char l) {
	        const static char vowels[] = {'a','e','i','o','u','y'};
	        for (int i=0; i<6; i++) {
	            if (l==vowels[i]) return true;
	        }
	        return false;
	    }
		bool chance(int min, int max) {
			int r = (rand() % max);
			if (r+1<=min)
				return true;
			else
				return false;
		}
	}
	std::string Faction::makename(unsigned char names) {
		if (names==0) names=language.namec;
        std::string fullname;
        for (int i=0;i<names;i++) {
            int nc = rand() % language.maxlen + language.minlen;
            std::string name;
            for (int z=0;z<nc;z++) {
            startover:
                std::string segment=language.syllables[rand()%language.syllablec];
                bool delFromName = false;
            start:
                if (name.size()>0) {
	                if (isVowel(segment[0]) ) {
	                    if (  (segment[0]==name[name.size()-1] && !language.longvowels)  ||
	                          (isVowel(name[name.size()-1]) && !language.vowelclusters ) ) {
	                        if (!delFromName && chance(1,3)) {
	                            if(!(name.size()>2)) goto startover;
	                            delFromName=true;
	                            name=name.substr(0,name.size()-2);
	                        } else {
	                            if(!(segment.size()>2)) goto startover;
	                            segment=segment.substr(1,segment.size()-1);
	                        }
	                        goto start; //I will not apologize.
						}
	                } else {
	                    if ( (segment[0]==name[name.size()-1] && !language.gemination) ) {
	                        segment=segment.substr(1,segment.size()-1);
	                    } else if (!isVowel(name[name.size()-1]) && !language.consonantclusters ) {
	                        if (!delFromName && chance(1,3)) {
	                            if(!(name.size()>2)) goto startover;
	                            delFromName=true;
	                            name=name.substr(0,name.size()-2);
	                        } else {
	                            if(!(segment.size()>2)) goto startover;
	                            segment=segment.substr(1,segment.size()-1);
	                        }
	                        goto start;
	                    }
	                }
				}
                if (segment.size()==0) goto startover;
                name+=segment;
            }
            name[0]-=0x20;
            fullname+=name;
			if (language.suffixc > 0) fullname+=language.suffixes[rand()%language.suffixc];
            if (i+1 != names) fullname += " ";
        }
        return fullname;
    }
}