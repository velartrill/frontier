#include "label.h"
namespace ui {
	Label::Label(std::string _text, float _x, float _y, float _w, float size, color::color _color, Font::face font):
		text(_text, _w, Font::align::left, font, size) {
		x=_x; y=_y; w=_w; color=_color; shadow=false; shadowcolor={0,0,0};
		alpha=1;
	}
	void Label::render() {
		float px = abx(), py = aby();
		if (alpha<1) glEnable(GL_BLEND);
		if (shadow) {
			glColor4f(shadowcolor.r, shadowcolor.g, shadowcolor.b, alpha);
			text.render(px,py-1);
		}
		glColor4f(color.r, color.g, color.b, alpha);
		text.render(px,py);
		if (alpha<1) glDisable(GL_BLEND);
	}
	void Label::loop(){};
	void Label::handle(const event&){};
	
}