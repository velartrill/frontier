#include "window.h"
#include "text.h"
#include "controllers/title.h"
#include "util.h"
#include "data/character.h"
#include <iostream>
#include <cstdlib>
int main(int argc, char** argv) {
	unsigned int width, height;
	if (argc == 3) {
		width=atoi(argv[1]);
		height=atoi(argv[2]);
	} else {
		width=1024;
		height=640;
	}
	try {
		window::init(width,height);
		window::controller = new Controllers::Title;
		window::mainloop();
	} catch (const char* e) {
		std::cout<<"\x1b[31;1m[ EE ]\x1b[0m "<<e<<'\n';
		// pretty much just using exceptions to display errors to the player.
	}
}