#pragma once
#include "../ui.h"
#include "../ui/statbar.h"
#include "../ui/label.h"
#include "../ui/button.h"
#include "../data/ship.h"
#include "../data/map.h"
#include "../form.h"
#include "../image.h"
namespace Controllers {
	class Shipyard : public ui::Form {	// FIXME just using the Form for a nice background until I
										// can be arsed to give it a decent style.
		data::system* sys;
		data::ship* ship;
		ui::Label l_shipName, l_shipClassName, l_shipClassDesc;
		Image im_ship;
		ui::StatBar s_shipEnergy;
			ui::StatBar::Series sr_shipEnergy[2];
			ui::Button	b_recharge, b_rechargemax,
						b_hyperdrive;
			data::price_t getRechargePrice(data::health_t energy) const;
			void updateprices(), updatestats();
		public:
			//void render() override;
			Shipyard(data::system*,data::ship*);
			void callback(void*w) override;
			void handle(const ui::event&) override;
			void render() override;
	};
}