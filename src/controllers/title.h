#pragma once
#include "../ui.h"
#include "../window.h"
#include "../ui/button.h"
#include "../ui/label.h"
#include "../ui/field.h"
#include "../image.h"
#include "../form.h"
namespace Controllers {
	class Title : public ui::Controller {
		class NewGame : public ui::Form { public:
			ui::Field i_name, i_ship;
			ui::Button b_gender_nonbinary, b_gender_female, b_gender_male, b_next;
			ui::Button::Group b_gender;
			public:
				NewGame();
				void callback(void*) override;
				/* DEBUG CODE - Speed up getting past the first screen. REMOVE in production */
					void auxcallback(void*) override;
				/* END DEBUG CODE */
		};
		ui::Button b_newgame, b_quit;
		Image logo;
		public:
			Title();
			void render() override;
			void callback(void*) override;
	};
}