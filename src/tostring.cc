#include "tostring.h"
#ifdef UNIT_TEST
#include <iostream>
int main() {
	struct Pair {std::string str1; std::string str2;};
	Pair pairs[] = {
		{"BOB","bob"},
		{"bob","BOB"},
		{"BOB","BOB"},
		{"bOb","BoB"},
		{"bob","bob"},
		{"dave","ahmad"},
		{"fritz","ahmad"},
		{"bob","emperor gorblesnax the deranged"}
	};
	for (Pair& p : pairs) {
		if (stricmp(p.str1, p.str2))
			std::cout<<p.str1<<" matches "<<p.str2<<'\n';
		else
			std::cout<<p.str1<<" does not match "<<p.str2<<'\n';
	}
}
#endif

//case-insensitive comparison for C++ strings
bool stricmp(std::string& str1, std::string& str2) {
	if (str1.size() != str2.size()) return false;
	for (size_t i = 0; i < str1.size(); i++) {	// true story: after years of writing FOR statements, I managed to get the order
												// of the second and third parameters wrong. BEHOLD THE GENIUS THAT IS ALEXIS HALE
		if (str1[i] != str2[i]
			&& str1[i] - 0x20 != str2[i]
			&& str1[i] + 0x20 != str2[i]) return false;
	}
	return true;
}
