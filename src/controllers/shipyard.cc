#include "shipyard.h"
#include "../window.h"
#include "../data/types.h"
#include "../data/universe.h"
#include "../strings.h"
#include "../alert.h"
#include "buyhyperdrive.h"
namespace Controllers {
	Shipyard::Shipyard(data::system* _sys, data::ship* _ship) :
		l_shipName		(_ship->name,			20,	window::height-155,	window::width-350,		36.0f),
		l_shipClassName	(_ship->shipclass->name,20,	window::height-185,	window::width-350,		20.0f),
		l_shipClassDesc	(_ship->shipclass->desc,window::width-350,  l_shipName.y,	350,	15.0f),
		im_ship			(_ship->shipclass->image.file,
						 _ship->shipclass->image.w,
						 _ship->shipclass->image.h),
		s_shipEnergy	(						10,					10,	window::width-500,	50),
		b_recharge		("",	s_shipEnergy.w+	20,					10,	295,				50),
		b_rechargemax	("",	s_shipEnergy.w+315,					10,	170,				50, &color::green),
		b_hyperdrive	("Hyperdrives", window::width-220, 		   90, 200,				60,	&color::brown)
	{
		addtitle("Shipyard");
		sys		=	_sys;
		ship	=	_ship;

		if (sys->faction) color=sys->faction->color;
		s_shipEnergy.max = ship->shipclass->energy;
		sr_shipEnergy[1].name="Current energy";
		sr_shipEnergy[1].color=&color::green;
		sr_shipEnergy[0].name="Amount to recharge";
		sr_shipEnergy[0].color=&color::purple;
		s_shipEnergy.seriesc=2;
		s_shipEnergy.series=sr_shipEnergy;
		updatestats();
		updateprices();

		l_shipClassName.alpha=.7;

		add(&l_shipName);
		add(&l_shipClassName);
		add(&l_shipClassDesc);
		add(&s_shipEnergy);
		add(&b_recharge);
		add(&b_rechargemax);
		
		add(&b_hyperdrive);
	}
	data::price_t Shipyard::getRechargePrice(data::health_t energy) const {
		if (energy<=ship->energy) return 0;
		return (energy-ship->energy)*(10-sys->techlevel/2);
	}
	void Shipyard::updateprices() {
		b_recharge.settext("Recharge ("+strings::price(getRechargePrice(sr_shipEnergy[0].val))+")");
		b_rechargemax.settext("Max ("+strings::price(getRechargePrice(ship->shipclass->energy))+")");
	}
	void Shipyard::updatestats() {
		sr_shipEnergy[1].val=ship->energy;
		sr_shipEnergy[0].val=0;
		if (ship->energy == ship->shipclass->energy) {
			b_recharge.visible=false;
			b_rechargemax.visible=false;
		} else {
			b_recharge.visible=true;
			b_rechargemax.visible=true;
		}
	}
	void Shipyard::handle(const ui::event& e) {
		if (e.type==ui::eventtype::key && e.key.state==ui::pressed && e.key.key==GLFW_KEY_ESC) window::back();
		else Form::handle(e);
	}
	void Shipyard::callback(void* w) {
		if (w==&s_shipEnergy) {
			sr_shipEnergy[0].val=std::min(((data::health_t)s_shipEnergy.lastClick)+1, ship->energy+(ship->shipclass->energy - ship->energy));
			updateprices();
		} else if (w==&b_recharge) {
			if (sr_shipEnergy[0].val > ship-> energy) {
				data::price_t price = getRechargePrice(sr_shipEnergy[0].val);
				if (price > data::player->money)
					window::alert("You can't afford to recharge your ship that much.",&color::red);
				else {
					data::player->spend(price);
					ship->energy=sr_shipEnergy[0].val;
					window::alert("Ship recharged.", &color::green);
					updatestats();
					updateprices();
				}
			} else window::alert("No recharge amount selected.",&color::red);
		} else if (w==&b_rechargemax) { // woo, copy-paste generics
			data::price_t price = getRechargePrice(ship->shipclass->energy);
			if (price > data::player->money)
				window::alert("You can't afford to recharge your ship completely.",&color::red);
			else {
				data::player->spend(price);
				ship->energy=ship->shipclass->energy;
				window::alert("Ship fully recharged.", &color::green);
				updatestats();
				updateprices();
			}
		} else if (w==&b_hyperdrive) {
			window::load(new BuyHyperdrive);
		}
	}
	void Shipyard::render() {
		Form::render();
		glEnable(GL_BLEND);
		im_ship.render(20,(window::height-im_ship.h)-200);
		glDisable(GL_BLEND);
	}
} 