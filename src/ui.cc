#include "ui.h"
#include "window.h"
namespace ui {
//
// WIDGET
//
	Widget::Widget() {
		visible=true;
		state=off;
	}
	bool Widget::inhitbox(float x, float y) {
		return false;
	}
	float Widget::abx() const {
		if (parent==nullptr) return x;
		else return x+parent->abx();
	}
	float Widget::aby() const {
		if (parent==nullptr) return y;
		else return y+parent->aby();
	}
	Widget::~Widget(){}
	void Widget::onBlur(){};
//	
// GROUP
//
	Group::Group() {
		hoverwidget=nullptr;
		activewidget=nullptr;
		focuswidget=nullptr;
	}
	void Group::render() {
		for (Widget* w : widgets) w->render();
	}
	void Group::loop() {
		for (Widget* w : widgets) w->loop();
	}
	void Group::add(Widget* w) {
		widgets.push_back(w);
		w->parent=this;
	}
	void Group::defocus() {
		focuswidget->onBlur();
		focuswidget->state=off;
		focuswidget=nullptr;
		if (parent!=nullptr) {
			parent->defocus();
		}
	}
	void Group::focusOn(Widget* w) {
		focuswidget=w;
		w->state=focus;
		activewidget=nullptr;
		hoverwidget=nullptr;
		if (parent!=nullptr) {
			parent->focusOn(this);
		}
	}
	void Group::callback(void* v){if (parent!=nullptr) parent->callback(v);}
	void Group::auxcallback(void* v){if (parent!=nullptr) parent->auxcallback(v);}
	void Group::handle(const event& e) {
		begin_handle: switch (e.type) {
			case eventtype::motion:
				if (focuswidget != nullptr && focuswidget->inhitbox(e.motion.x,e.motion.y)) {
					focuswidget->handle(e);
				} else if (activewidget==nullptr) {
					if (hoverwidget==nullptr) search: for (auto wid = widgets.rbegin(); wid!=widgets.rend(); wid++) {
						Widget* w = *wid;
						if (w->visible && w->inhitbox(e.motion.x,e.motion.y)) {
							w->state=hover;
							hoverwidget=w;
							w->handle(e);
						}
					} else {
						if (!(hoverwidget->inhitbox(e.motion.x,e.motion.y))) {
							hoverwidget->state=off;
							hoverwidget=nullptr;
							goto search;
						} else {
							hoverwidget->handle(e);
						}
					}
				} else {
					if (!(activewidget->inhitbox(e.motion.x,e.motion.y))) {
						activewidget->state=hover;
						activewidget->handle(e);
						hoverwidget=nullptr;
					} else {
						activewidget->state=active;
						activewidget->handle(e);
						hoverwidget=activewidget;
					}
				}
			break;
			case eventtype::click:
				if (focuswidget!=nullptr) {
					if (focuswidget->inhitbox(window::mousex,window::mousey)) {
						//yes, the above is repulsive, relying on global state and all.
						//maybe one day I'll fix it.
						focuswidget->handle(e);
					} else {
						focuswidget->onBlur();
						focuswidget -> state = off;
   						focuswidget = nullptr;
   						goto begin_handle;
   					}
				} else if (e.click.state==ui::pressed) {
					if (hoverwidget != nullptr) {
						activewidget=hoverwidget;
						activewidget->state=active;
						activewidget->handle(e);
					}
				} else {
					if (hoverwidget != nullptr && activewidget != nullptr) {	/* the widget has been clicked */
						// the second check is needed to avert a rare, mysterious segfault indicative of bad UI logic... somewhere.
						// So, FIXME.
						activewidget->state=hover;
						activewidget->handle(e);
						activewidget=nullptr;
					} else {
						if (activewidget != nullptr) {
							activewidget->handle(e);
							activewidget->state=off;
							activewidget=nullptr;
						}
					}
					// generate a fake motion event to check what the user put the mouse atop. hackish.
					event m; m.type=ui::eventtype::motion; m.motion.x=window::mousex; m.motion.y=window::mousey;
					// FIXME dx and dy unset
					handle(m);
				}
			break;
			case eventtype::key: case eventtype::text:
				if (focuswidget!=nullptr) focuswidget->handle(e);
			break;
			default:
				if (focuswidget!=nullptr) focuswidget->handle(e);
				else if (activewidget!=nullptr) activewidget->handle(e);
				else if (hoverwidget!=nullptr) hoverwidget->handle(e);
		}
	}
	bool Group::inhitbox(float px, float py) {
		return px>=abx() && px<=abx()+w && py>=aby() && py<=aby()+h;
	}
	bool ReverseGroup::inhitbox(float px, float py) {
		return px>=abx() && px<=abx()+w && py<=aby() && py>=aby()-h;
	}
	Group::~Group(){}
//
// CONTROLLER
//
	Controller::Controller() {
		parent=nullptr;
		x=0;
		y=0;
	}
	bool Controller::inhitbox(float x, float y) {
		return true;
	}
	void Controller::reactivate(void*){}
	Controller::~Controller(){}
}