#pragma once
#include <string>
#include "../ui.h"
#include "../text.h"
#include "../color.h"
namespace ui {
	class Label : public Widget { public:
		color::color color, shadowcolor;
		bool shadow;
		TextBox text;
		float alpha;
		float w;
		Label(std::string text, float x, float y, float w, float size=20.0f, color::color color={1,1,1}, Font::face font = Font::face::zekton);
		void render();
		void loop();
		void handle(const event&);
	};
};