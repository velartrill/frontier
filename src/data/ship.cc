#include "ship.h"
#include <cassert>
#include "map.h"
#include "../ptdist.h"
#include "quantity.h"
namespace data {
	ship::ship(ship::Class* cl, std::string _name) {
		shipclass=cl;
		hull=cl->hull;
		energy=cl->energy;
		items.push_back(*cl->hyperdrive);
		hyperdrive=&items.back();
		name=_name;
	}
	weight_t ship::weight() const {
		weight_t w = 0;
		for (const inventory<commodity*>::quantity& c : commodities.items) {
			w += c.ct->weight * c.q;
		}
		return w;
	}
	dist_t ship::range() const {
		if (hyperdrive!=nullptr) return hyperdrive->range;
		else return 0;
	}
	health_t ship::jumpEnergyCost(dist_t dist) const {
		// Hyperjump fuel formula
		return shipclass->size*3*dist * hyperdrive->rate;
	}
	health_t ship::jumpEnergyCost(const system* dest) const {
		// Hyperjump fuel formula
		return jumpEnergyCost(distanceTo(dest));
	}
	dist_t ship::distanceTo(const system* dest) const {
		data::system& start = *getSystem();
		return ptdist(start.x,start.y,dest->x,dest->y);
	}
	utime_t ship::jumpTimeTo(dist_t dist) const {
		assert (hyperdrive!=nullptr);
		utime_t t = std::ceil(dist*14.4 / hyperdrive->speed); // 14.4 is the Hyperfield Pinch Constant. Why? DON'T ASK FOOLISH QUESTIONS
		return t;
	}
	utime_t ship::jumpTimeTo(const system* dest) const {
		return jumpTimeTo(distanceTo(dest));
	}
	bool ship::canJumpTo(const system* dest) const {
		if (loc.type!=location_t::Type::system) return false;
		if (hyperdrive==nullptr) return false;
		const float dist = distanceTo(dest);
		return (dist <= range() && jumpEnergyCost(dist) <= energy);
	}
	void ship::jumpTo(const system* s) {
		assert(canJumpTo(s));
		energy-=jumpEnergyCost(s);
		loc.type = data::location_t::Type::system; // redundant, buuut leaving in just in case
		loc.loc = (data::location*)s;
	}
}