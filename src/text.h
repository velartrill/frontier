#pragma once
#include <string>
#include <FTGL/ftgl.h>
#include "font.h"
namespace Font {
	void init();
	enum class face { header, body, zekton, title };
	enum class align { left = 0, center, right, justify };
	
}

class Text { public:
	std::string str;
	Font::face font;
	float size;
	Text(std::string, Font::face = Font::face::zekton, float size = 24.0f);
	void render(float x, float y);
	float width();
};
class PosText : public Text { public:
	// using Text::Text; // clang no likey!
	inline PosText(std::string s, Font::face face = Font::face::zekton, float size = 24.0f) : Text(s,face,size){}
	float x, y;
	inline void posrender() {render(x,y);}
};

// wrapper for FTSimpleLayout class
class TextBox { public:
	float size;
	std::string str;
	FTSimpleLayout box;
	FR_FTGL_FONT_TYPE* font;
	TextBox(std::string str, float w, Font::align = Font::align::left, Font::face = Font::face::zekton, float size = 12.0f);
	void render(float x, float y);
	inline void align(Font::align a) {
		box.SetAlignment((FTGL::TextAlignment)a);
	}
	float width();
	float height();
};