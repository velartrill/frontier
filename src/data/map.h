#pragma once
#include <list>
#include "ship.h"
#include "types.h"
#include "location.h"
#include "faction.h"
#include "condition.h"
#define MAX_ORBIT 50.0f
#define MIN_ORBIT 10.0f
namespace data {
	struct planet : public location {
		std::string name;
		orbit_t orbit;
		unsigned char moonc;
		planet* moons;
		float size;
		tech_t techlevel; // limited by system tech level
		enum class Type {mercurial=0, oceanic, grassy, barren, gasgiant} type;
		inline color::color color() const {
			switch (type) {
				case Type::mercurial:	return {1,.3,.3}; break;
				case Type::oceanic: 	return {.2,.8,1}; break;
				case Type::grassy: 		return {.4,.9,.5}; break;
				case Type::barren: 		return {.4,.4,.4}; break;
				case Type::gasgiant:	return {1,.8,.3}; break;
			}
		}
		inline bool habitable() const {
			return type==Type::oceanic || type==Type::grassy;
		}
	};
	struct system : public location {
		enum class startype { yellowdwarf = 0, redgiant, bluegiant };
		std::string name;
		std::list<data::condition> conditions;
		std::list<data::commodity*> products;
		dist_t x, y;
		float size;
		unsigned char planetc;
		planet*	planets;
		tech_t techlevel;
		float marketcontrol;
		Faction* faction;
		inline startype type() const {
			return	size < .7	? startype::yellowdwarf :
					size < 1.2	? startype::redgiant	:
					startype::bluegiant;
		}
		void marketTick();
		void resetMarket();
		bool habitable();
		bool afflictedBy(condition::prototype*) const;
		struct Type {
			const char* name;
			color::color color;
		};
		static const Type types[];
		// struct factor {
		// 	enum Type {violence, drought, epidemic, war, starvation} type;
		// 	float severity;
		// };
		// std::list<factor> factors;
	};
}