#pragma once
#include "types.h"
#include "commodity.h"
/// Describes conditions that can affect the market in various systems.
namespace data {
	class system;
	struct condition {
		struct prototype {
			const char* name;
			utime_t (*affect_det)(system*,unsigned int);
			unsigned int affect_det_param;
			void* (*operand_det)(system*);
			unsigned char probability;
		};
		prototype* proto;
		utime_t timeleft;
		void* operand;
	};
	namespace conditions {
		extern condition::prototype
			war,
			anarchy,
			unrest,
			drought,
			famine,
			shortages,
			bureaucracy,
			overpopulation,
			marketcollapse,
			bubble;
		extern condition::prototype* all[];
		extern const unsigned char count;
	}
}