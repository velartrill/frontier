#include "systemmap.h"
#include "../window.h"
#include <glfw.h>
#include "../circle.h"
#include "../util.h"
#include "merchant.h"
#include "shipyard.h"
#include "../data/universe.h"
namespace Controllers {
	SystemMap::SystemMap(data::system* s):
		i_ship("ui/ship.tga",64,64,true),		b_ship("Shipyard", window::width-180,window::height-74, &i_ship),
		i_trade("ui/trade.tga",64,64,true),		b_trade("Trade", b_ship.x+i_ship.w+5,b_ship.y, &i_trade),
		i_sysinfo("ui/sysinfo.tga",64,64,true),	b_sysinfo("System Information", b_ship.x-67,b_ship.y,&i_sysinfo),
		
		l_system	(s->name.c_str(),
						20,window::height-46,window::width,36.0f),
		l_faction	((s->faction!=nullptr ? s->faction->name : ""),
						20,window::height-(46+12+18),window::width,18.0f)
		//l_trade		("Trade",20,20,window::width,48.0f)
	{
		sys=s;
		if (s->faction!=nullptr) l_faction.color = s->faction->color->base.fg, l_faction.alpha=.7;
		add(&l_system);
		add(&l_faction);
		// add(&l_trade);
		drag=0;
		dragging=false;
		// dragUpdate();
		if(s->habitable()) {
			add(&b_ship);
			add(&b_trade);
			add(&b_sysinfo);
		}
	}
	// void SystemMap::dragUpdate() {
	// 	l_trade.alpha=(drag > 0) ? (float)drag / (window::width/3) : 0;
	// 	if (drag!=0 && dragging==false) drag=0;
	// }
	SystemMap::posData SystemMap::getPosData() {
		return {(float)window::width/2 + drag,
				(float)window::height/2,
				((float)window::smallest_dimension()/2)/std::max(MAX_ORBIT, sys->planets[sys->planetc-1].orbit),
				std::min(1.0f,(std::max(MAX_ORBIT, sys->planets[sys->planetc-1].orbit)/2)/MAX_ORBIT)};
	}
	void SystemMap::render() {
		posData c = getPosData();
		color::color cl;
		switch (sys->type()) {
			case data::system::startype::yellowdwarf:	cl={.7,.7,.4};	break;
			case data::system::startype::redgiant:		cl={.7,.4,.4};	break;
			case data::system::startype::bluegiant: 	cl={.4,.6,.7};	break;
		}
		float fac = sys->size*10*c.sizefactor;
		glEnable(GL_BLEND);
		glColor3f(cl.r,cl.b,cl.g);
		draw::circleGrad(window::width/2, window::height/2, fac*10, cl,1, cl, 0, 10);

		/* draw the center */
		glEnable(GL_MULTISAMPLE);
		glColor3f(1,1,1);
		draw::circle(window::width/2, window::height/2, fac*5, fac/2, true);
		for (int i=0; i<sys->planetc; i++) { data::planet& p = sys->planets[i];
			//if (sys->faction!=nullptr)	color::apply(*sys->faction->color, off, color::colortype::fg, .3);
			//					else	glColor4f(1,1,1,.5);

			color::color pc = p.color();
			glColor4f(pc.r,pc.g,pc.b,0.3);
			draw::circle(c.cx,c.cy,p.orbit*c.distfactor,2);
			glColor3f(pc.r,pc.g,pc.b);
			draw::circle(c.cx + p.orbit*c.distfactor, c.cy, p.size*c.distfactor, .5, true);

		}
		if (!sys->conditions.empty()) {
			PosText t("System Events");
			t.x=10; t.y=(sys->conditions.size()+1)*20 + 60;
			const float width = t.width() * 1.5;
			glBegin(GL_QUADS);
				glColor4f(.3,.3,1, 0.6);	glVertex2f(10,t.y);
				glColor4f(.3,.3,1, 0.0);	glVertex2f(width+10,t.y);
											glVertex2f(width+10,t.y+40);
											glVertex2f(10,t.y+40);
			glEnd();
			glBegin(GL_LINES);
				glColor4f(.5,.5,1,1);	glVertex2f(10.5,t.y+.5);
				glColor4f(.5,.5,1,0);	glVertex2f(width+10.5,t.y+.5);
			
				glColor4f(.5,.5,1,1);	glVertex2f(10.5,t.y+40.5);
				glColor4f(.5,.5,1,0);	glVertex2f(width+10.5,t.y+40.5);
				
				glColor4f(.5,.5,1,1);	glVertex2f(10.5,t.y+.5);
										glVertex2f(10.5,t.y+40.5);
			glEnd();
			glColor3f(1,1,1);
			t.x+=5, t.y += 10;
			t.posrender();
			t.y-=30; t.size=18; t.x=15;
			glDisable(GL_MULTISAMPLE);
			for (data::condition& c : sys->conditions) {
				t.str = c.proto->name;
				t.posrender();
				t.y-=20;
			}
			glEnable(GL_MULTISAMPLE);
		}
		glDisable(GL_BLEND);
		glDisable(GL_MULTISAMPLE);
		Controller::render();
	}
	// void SystemMap::loop() {
	// 	dragUpdate();
	// }
	void SystemMap::handle(const ui::event& e) {
		if (e.type==ui::eventtype::key && e.key.state==ui::pressed) switch (e.key.key) {
			case GLFW_KEY_ESC: window::back(); break;
			case 'T': if (sys->habitable()) callback(&b_trade); break; // I'm calling this one the "Microsoft Access" pattern
			case 'S': if (sys->habitable()) callback(&b_ship); break;
		// } else if (e.type==ui::eventtype::motion && dragging) {
		// 	drag+=e.motion.dx;
		} else {
			Controller::handle(e);
		}
		//  else if (e.type==ui::eventtype::click) {
		// 	dragging = e.click.state==ui::pressed;
		// 	if(!dragging && drag>window::width/3) window::load(new Merchant(sys));
		// }
	}
	void SystemMap::callback(void* w) {
		if (w==&b_trade) window::load(new Merchant(sys));
		else if (w==&b_ship) window::load(new Shipyard(sys,data::player->getShip()));
	}
}