namespace data {
	const char* aegis_syllables[] = {
		"ha", "tur", "ria", "suan", "mora", "lia", "mosan", "suvus", "isa", "lowath", "waris",
		"ayya", "murash", "ash", "shara", "mios", "noai", "ila", "foran", "kira", "kir", "dura",
		"iram", "chasa", "chai", "shios", "shio", "shi", "cha", "kir", "rike", "rayak", "raya",
		"lea", "mor", "bir", "an", "dov", "dur", "lia", "ruan","aqri","eqra","qan","uqra","uq",
		"aq","qir","qur","shaq","shaqro","enu","eshi","ashti","tir","hast","hash","rahus","shihar",
		"ayriq","ayqri","qay","may","may","rui","ashray","sharya","kuras","kasrash","thal","sumath",
		"ayth","ayath","shath","iath"
	}; //75
	const char* empire_syllables[] = {
		"lus", "tras", "pomp", "bor", "bord", "cyr", "sev", "ignor", "ignord", "illus", "illust",
		"illustra", "illius", "sit", "vor", "vord", "dar", "bus", "pus", "tor", "tord", "vad",
		"bar", "bard", "stult", "it", "or", "ord", "ant", "int", "inv", "not", "agri", "agra",
		"laud", "claud", "anor", "anord", "dex", "ex", "ax", "ix", "an", "vax", "un", "jup", "sat",
		"urn", "anus", "mar", "ero", "nea", "cles", "mer", "merc", "sex", "ven", "jer", "qua", "quo",
		"qui", "quand", "qua", "quius", "equius", "quor", "num", "sum", "quid", "quis", "quim", "quia",
		"quod", "cleo", "cleon", "clit", "cliam", "cus", "cul", "dol", "rem", "ips", "exi", "bul",
		"ves", "est", "agna", "nim", "lis", "ali", "rex", "mult"
	}; //92
	const char* empire_suffixes[] = {
		"it", "ito", "itor", "ilis", "ialis", "is", "aeus", "ae", "i", "ii", "ia", "o", "a", "ius",
		"us", "um", "endum", "abus", "ibus", "orus", "iam", "e", "er", "ex", "atrix", "tius", "tio",
		"agus", "amus", "rix", "vex", "nunc", "hoc", "ver", "tri", "ulus"
	}; //36
	const char* society_syllables[] = {
		"sar", "sarc", "sarm", "lasa", "taso", "sho", "elle", "ette", "lum", "liere", "es", "se",
		"ques", "sia", "siac", "au", "auc", "o", "vo", "ven", "ri", "ric", "forun", "forunc",
		"for", "sla", "slac", "mus", "mal", "ser", "serc", "serm", "sa", "as", "quas","tel", "tir",
		"nes","nol","lan","tela","taros","minal","lor","ve","sle","slo","sir","sli","asla","asle"
	}; //55
}
