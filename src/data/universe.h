#pragma once
#include "map.h"
#include "character.h"
#include "types.h"
#include "item.h"
#include "commodity.h"
#include <list>
namespace data {
	extern character*	player;
	extern system*		systems;
	extern utime_t		utime;
	extern dist_t		usize;
	extern unsigned int	systemc;
	extern const tech_t	maxtech;
	extern void generateUniverse(std::string shipname);
	extern std::list<item>	items;
	extern void tick(utime_t);
	extern void cleanupUniverse();
}