#pragma once
#include "types.h"
#include <stdlib.h>
namespace data {
	class planet;
	class system;
	struct commodity {
		const char*	name;
		quantity_t (*quantity_det)(const system*,unsigned int param); // called to determine where this is found
		unsigned int quantity_det_param;
		rarity_t	rarity; // price
		weight_t	weight;
		float		(*price_det)(const system*, unsigned int param, bool* use); // let the commodity figure out its own price multiplier
		unsigned int price_det_param;
		bool		(*tradable_det)(const system*, unsigned int param);
		unsigned int tradable_det_param;
		quantity_t	(*minable_det)(const system*, const planet*, rarity_t);
		const char* desc;
		
		price_t priceIn(const system*) const;
		quantity_t quantityIn(const system*) const;
		inline bool tradableIn(const system* s) {
			return tradable_det ? (*tradable_det)(s,tradable_det_param) : true;
		}
		inline quantity_t minable(const system* s, const planet* p) {
			return (*minable_det)(s,p,rarity);
		}
	};
	extern commodity	commodities[];
	extern const size_t	commodityc;
}
