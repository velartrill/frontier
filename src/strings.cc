#include "strings.h"
#include "tostring.h"
namespace strings {
	template <typename T> std::string bignum(T v) {
		std::string in = toString(v);
		if (in.size()<=3) return in;
		std::string out = "";
		unsigned char count = 0;
		for (int i = in.size()-1; i>=0; i--) {
			out=in[i]+out;
			if (count==2) {
				count=0;
				if (i!=0) out=','+out;
			} else count++;
		}
		return out;
	}
	std::string price(data::price_t v)		{	return bignum(v) + " Cr.";	}
	std::string balance(data::balance_t v)	{	return bignum(v) + " Cr.";	}
	std::string weight(data::weight_t v)	{	return bignum(v) + " kg";	}
	std::string date(data::utime_t t) {
		using namespace data::calendar;
		return std::string(weekday(t)) + ", " + toString<unsigned int>(day(t)) +
			' ' + months[month(t)] + ' ' + toString(year(t));
	}
	std::string timespan(data::utime_t t) {	return t==1 ? "1 day" : toString(t) + " days"; }
	std::string percentage(float f) {return toString(f*100) + "%"; }
	std::string techlevel(data::tech_t t) {
		std::string name;
		if (t>=17) {
			name="Ultratech";
		} else if (t>=13) {
			name="High-tech";
		} else if (t>=7) {
			name="Postindustrial";
		} else if (t>=5) {
			name="Industrial";
		} else if (t<5 && t!=0) {
			name="Agricultural";
		} else if (t==0) {
			name="Primitive";
		}
		return name + " ("+toString<unsigned int>(t)+")";
	}
	std::string distance(data::dist_t f) {return toString(f) + " parsecs"; }
	std::string hyperdrive_efficiency(float f) {return toString(10.0/(f*10.0)) + "x"; }
}