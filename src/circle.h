#pragma once
#include <glfw.h>
#include "color.h"
namespace draw {
	extern void circle(float x, float y, float radius, float quality=1, bool filled=false);
	extern void circleGrad(float x, float y, float radius,
		const color::color& inner, float inneralpha, const color::color& outer, float outeralpha,
		float quality=1);
}