#pragma once
#include <string>
#include "types.h"
namespace data {
	struct item {
		char* name;
		char* desc;
		enum class Type { weapon, shield, hyperdrive, misc } type;
		price_t price;
		tech_t techlevel;
		health_t effect; // weapons
		dist_t speed; // hyperdrive - measured in parsecs (dist_t) per day (tick)
		float rate; // /weapons, hyperdrive (energy usage *= efficiency)
		dist_t range; // weapons, hyperdrive
		utime_t recharge; // weapon
	};
	/* Because C++ is an ill-thought-out cesspool of effluent, waste, and decay,
	it's impossible to statically initialize something that contains a union.
	So we're just sharing fields here, which is evil beyond words. */
}