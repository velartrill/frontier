#include <glfw.h>
#include "window.h"
#include "util.h"
namespace window {
	unsigned int	width, height,
					mousex, mousey;
	int				scroll=0;
	extern void		renderAlert();
	bool	curAlert;
	double old_time;
	ui::Controller* controller;
	ui::Controller* stack[16]; // this will never actually be directly referenced,
	ui::Controller** stackptr = stack; // except here. it's just to allocate the space.
	ui::Controller* queued = nullptr;
	bool qback = false;
	static union {
		void* qbackrv;
		bool qstack;
	};
	void GLFWCALL handle_motion(int x, int y) {
		ui::event e;
		e.type=ui::eventtype::motion;
		e.motion.x=x;
		e.motion.y=window::height - y;
		e.motion.dx=x - window::mousex;
		e.motion.dy=e.motion.y - window::mousey;
		window::mousex=x;
		window::mousey=e.motion.y;
		controller->handle(e);
	};
	void GLFWCALL handle_key(int key, int state) {
		ui::event e;
		e.type=ui::eventtype::key;
		e.key.key=key;
		if (state==GLFW_PRESS) e.key.state=ui::pressed; else e.key.state=ui::released;
		controller->handle(e);
	};
	void GLFWCALL handle_text(int key, int state) {
		ui::event e;
		e.type=ui::eventtype::text;
		e.key.key=key;
		if (state==GLFW_PRESS) e.key.state=ui::pressed; else e.key.state=ui::released;
		controller->handle(e);
	};
	void GLFWCALL handle_click(int button, int state) {
		ui::event e;
		e.type=ui::eventtype::click;
		e.click.btn	= (	button == GLFW_MOUSE_BUTTON_LEFT ? ui::event_click::left:
						button == GLFW_MOUSE_BUTTON_MIDDLE ? ui::event_click::middle:
						/* button == GLFW_MOUSE_BUTTON_RIGHT ? */ ui::event_click::right );
						// woo ternary operator
		if (state==GLFW_PRESS) e.click.state=ui::pressed; else e.click.state=ui::released;
		controller->handle(e);
	};
	void GLFWCALL handle_scroll(int pos) {
		ui::event e;
		e.type=ui::eventtype::scroll;
		e.scroll.delta=pos-window::scroll;
		window::scroll=pos;
		controller->handle(e);
	}
	void init(unsigned int _width, unsigned int _height) {
		width=_width;
		height=_height;
		
		glfwInit();
		glfwOpenWindowHint(GLFW_FSAA_SAMPLES,8);
		glfwOpenWindow(width, height, 16,16,16,16, 0,0, GLFW_WINDOW);
		glfwSetWindowTitle("Frontier");
		glDisable(GL_MULTISAMPLE);
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glViewport(0,0,width,height);
		glOrtho(0,width,0,height,-1,1);
		glClearColor(0,0,0,1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		/*glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
		glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);*/
		
		old_time=glfwGetTime();
	};
	double ticks() {
		return glfwGetTime() - old_time;
	}
	void mainloop() {
		glfwSetMousePosCallback(handle_motion);
		glfwSetKeyCallback(handle_key);
		glfwSetCharCallback(handle_text);
		glfwSetMouseButtonCallback(handle_click);
		glfwSetMouseWheelCallback(handle_scroll);

		while (glfwGetWindowParam(GLFW_OPENED)) {
			if (qback) { // aaaarrgghhh, friggin' queues
				delete controller;
				controller=*--stackptr;
				controller->reactivate(qbackrv);
				qback=false;
			} else if (queued) {
				if (qstack) {
					*stackptr = controller;
					stackptr++; // look ma, no STL containers!
				} else {
					delete controller;
				}
				controller=queued;
				queued=nullptr;
			}
			controller->loop();

			old_time=glfwGetTime();
			glClear(GL_COLOR_BUFFER_BIT);
			controller->render();
			if (curAlert) {
				renderAlert();
			}
			glfwSwapBuffers();
		}
	};
	void load(ui::Controller* ct, bool stack) {
		queued=ct;
		qstack=stack;
	}
	void back(void* v) {
		qback=true; qbackrv=v;
	}
}