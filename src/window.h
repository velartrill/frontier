#pragma once
#include "ui.h"
namespace window {
	extern ui::Controller* controller;
	extern bool curAlert;
	extern void init(unsigned int width, unsigned int height),
		mainloop(), load(ui::Controller*, bool stack=true), back(void* = nullptr);
	extern unsigned int	width, height,
						mousex, mousey;
	extern double ticks();
	inline int smallest_dimension() {
		//return std::min(window::height, window::width);
		return (window::height < window::width ? window::height : window::width);
	}
}